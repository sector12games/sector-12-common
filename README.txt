Various shared dependencies for projects developed by Sector 12 Games.

As of 2016/10/23, the sector-12-lib and sector-12-common repos have been merged into one. The lib/ folder contains all 3rd party tools and the src/ folder contains all code developed in-house.
