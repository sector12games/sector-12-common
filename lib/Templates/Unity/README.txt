You will need to copy over your code templates for every new unity version, since
unity does not have an upgrade feature (you completely uninstall and re-install, or
install two versions side by side).

Replace files in this directory:
C:\Program Files (x86)\Unity\Editor\Data\Resources\ScriptTemplates

For more info:
http://answers.unity3d.com/questions/120957/change-the-default-script-template.html