How to modify and save resharper live templates:
  - Resharper -> Templates Explorer
  - "File Templates" tab
  - Modify and save your desired templates. They will be saved to your local "personal" resharper settings.
  - Resharper -> Manage Options
  - Select your personal layer, right click it
    - Copy Settings To -> ResharperGlobal (or whatever the name of your global settings file is)

How to modify stylecop company info:
  - Double click your global StyleCop.settings file
  - Company Information tab