A useful library for creating linq queries on the fly. It was released by Microsoft as part of a samples/learning zip file.
It was originally posted by Scott Gu. I've extracted the "DynamicQuery" project from the complete samples zip and
saved it here for future reference. Include Dynamic.dll in your project to use.

Used by RyamarAdmin.

http://weblogs.asp.net/scottgu/dynamic-linq-part-1-using-the-linq-dynamic-query-library
http://stackoverflow.com/questions/3991108/where-can-i-find-the-system-linq-dynamic-dll