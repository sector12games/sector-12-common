﻿# In order to use this assembly, you'll need to create a schema, and 
# run this script.  In MySql query browser, you can run scripts by
# going to File -> new script tab.  If using roles, first read my 
# note about indexes below.  It doesn't matter what your schema
# name is, as long as you update your ConnString in web.config 
# (or wherever) to point to the right schema.  The MySql version this
# script was tested on is 5.1.41.
# -ssb
CREATE TABLE Roles
(
  Rolename Varchar (255) NOT NULL,
  ApplicationName varchar (255) NOT NULL
);

CREATE TABLE UsersInRoles
(
  Username Varchar (255) NOT NULL,
  Rolename Varchar (255) NOT NULL,
  ApplicationName Text (255) NOT NULL
);

# These do not work with the current version of mysql.  If
# we ever really need to use roles, it would be a good
# idea to do a little research, and see if adding indexes
# like these are really necessary, or if there are any
# others that could be added as well to improve performance
#ALTER TABLE 'usersinroles' ADD INDEX ( 'Username', 'Rolename', 'ApplicationName')
#ALTER TABLE 'roles' ADD INDEX ( 'Rolename' , 'ApplicationName' ) ;

CREATE TABLE users (
  `PKID` VARCHAR(36) NOT NULL,
  `Username` VARCHAR(255) NOT NULL,
  `ApplicationName` VARCHAR(100) NOT NULL,
  `Email` VARCHAR(100) NOT NULL,
  `Comment` VARCHAR(255) NOT NULL,
  `Password` VARCHAR(128) NOT NULL,
  `PasswordQuestion` VARCHAR(255) NOT NULL,
  `PasswordAnswer` VARCHAR(255) NOT NULL,
  `IsApproved` TINYINT UNSIGNED NOT NULL,
  `LastActivityDate` DATETIME NOT NULL,
  `LastLoginDate` DATETIME NOT NULL,
  `LastPasswordChangedDate` DATETIME NOT NULL,
  `CreationDate` DATETIME NOT NULL,
  `IsOnline` TINYINT UNSIGNED NOT NULL,
  `IsLockedOut` TINYINT UNSIGNED NOT NULL,
  `LastLockedOutDate` DATETIME NOT NULL,
  `FailedPasswordAttemptCount` INTEGER UNSIGNED NOT NULL,
  `FailedPasswordAttemptWindowStart` DATETIME NOT NULL,
  `FailedPasswordAnswerAttemptCount` INTEGER UNSIGNED NOT NULL,
  `FailedPasswordAnswerAttemptWindowsStart` DATETIME NOT NULL,
  PRIMARY KEY (`PKID`)
)
ENGINE = InnoDB;