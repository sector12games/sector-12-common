This is a "membership and role" provider for use in asp.net and MySQL. ASP.NET membership gives you a 
built-in way to validate and store user credentials. This custom membership dll allows you to use
the built-in membership functionality with MySQl.

This code was created by Rakotomalala Andriniaina, over at CodeProject. At the time, no better,
free option existed. It's been quite some time since this code has been used, and better 
alternatives may now exist.

If this code is ever used again, I would recommend renaming the project and dll to "MySqlMembershipAndRoleProviders".
It was confusing coming back years later, and trying to figure out what a "CustomMysqlProvider" was.

This dll was used by my GameAdmin website (the old version of RyamarAdmin). It is no longer used,
as I've switched over to SqlSever.

http://www.codeproject.com/Articles/12301/Membership-and-Role-providers-for-MySQL