﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InverseBooleanConverter.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Sector12Common.Converters
{
    using System;
    using System.Linq;
    using System.Windows.Data;

    /// <summary>
    /// <para>
    /// Lets you bind to the inverse of a boolean in xaml.
    /// </para>
    /// <para>
    /// Stolen from a solution on stackoverflow:
    /// http://stackoverflow.com/questions/1039636/how-to-bind-inverse-boolean-properties-in-wpf
    /// </para>
    /// </summary>
    [ValueConversion(typeof(bool), typeof(bool))]
    public class InverseBooleanConverter : IValueConverter
    {
        /// <summary>
        /// Converts a boolean to its inverse.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns>
        /// The inverse of the boolean's value.
        /// </returns>
        public object Convert(
            object value, 
            Type targetType, 
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            if (targetType != typeof(bool))
                throw new InvalidOperationException("The target must be a boolean");

            return !(bool)value;
        }

        /// <summary>
        /// Converts a boolean's inverse value back to it's original.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns>
        /// The inverse of the boolean's value.
        /// </returns>
        public object ConvertBack(
            object value, 
            Type targetType, 
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
