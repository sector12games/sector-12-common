﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LevelToIndentConverter.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Sector12Common.Converters
{
    using System;
    using System.Linq;
    using System.Windows.Data;

    /// <summary>
    /// Class definition for LevelToIndentConverter.cs.
    /// </summary>
    public class LevelToIndentConverter : IValueConverter
    {
        /// <summary>
        /// Converts a tree level to an indent value.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns>
        /// The indent value, in pixels.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (int)value * 16;
        }

        /// <summary>
        /// Indention values should only be one way. ConvertBack is not supported.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns>
        /// A new NotSupportedException.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException("Not supported.");
        }
    }
}
