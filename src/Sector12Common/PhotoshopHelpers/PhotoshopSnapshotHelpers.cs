﻿// -----------------------------------------------------------------------
// <copyright file="PhotoshopSnapshotHelpers.cs" company="Sector12">
//     Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Sector12Common.PhotoshopHelpers
{
    using System;
    using System.Linq;
    using Photoshop;

    /// <summary>
    /// <para>
    /// These photoshop methods were designed for use with Adobe Photoshop CC (2013).
    /// Many of these methods use Photoshop ScriptListeners.
    /// </para>
    /// <para>
    /// "ScriptingListener" is a plugin that used to ship with all photoshop editions.
    /// As of CS6 and above, it is now a separate download. You can get it and read 
    /// more about where to put it here:
    /// http://www.adobe.com/devnet/photoshop/scripting.html
    /// http://forums.adobe.com/thread/1237032
    /// </para>
    /// <para>
    /// The Javascript/AppleScript/VBSCript DOM cannot do everything that photoshop
    /// can. It can handle most of the basic operations, but cannot do many things
    /// like create snapshots, execute filters from the filter gallery, etc.
    /// The developers of the Photoshop DOM and interop stuff knew this, so they
    /// also created the "ScriptingListener" plugin. You can use the plugin while
    /// Photoshop is running. Every command you execute will be show as a fairly
    /// cryptic piece of code that uses Actions and Ids to execute commands in
    /// photoshop. These actions are how photoshop is actually executing operations
    /// behind the scenes, and how things get saved in Photoshop's history. 
    /// Essentially, anything you can do in photoshop that will generate a history
    /// item in the history window can be performed using ScriptingListener code.
    /// Here's a fairly outdated, but still good tutorial on how to use the plugin.
    /// Don't pay attention to the installation stuff, as the plugin is no longer
    /// shipped with Photoshop. It's a separate download.
    /// http://blogs.adobe.com/crawlspace/2006/05/installing_and_1.html
    /// </para>
    /// <para>
    /// When dealing with ScriptingListener code, there are many different character
    /// and integer codes that are passed around. A very good reference to see what
    /// these codes mean can be found in the link below. Scroll down a bit and you
    /// will see all of the mappings. I discovered this link thanks to the second
    /// link below (the forum discussion).
    /// http://ps-scripts.cvs.sourceforge.net/viewvc/ps-scripts/xtools/xlib/PSConstants.js
    /// http://forums.adobe.com/thread/578130
    /// </para>
    /// </summary>
    public static class PhotoshopSnapshotHelpers
    {
        /// <summary>
        /// <para>
        /// Creates a snapshot in Photoshop history. This snapshot can be reverted
        /// to at a later time. 
        /// </para>
        /// <para>
        /// Creating snapshots is not directly supported by interop code. You must
        /// use ScriptingListener code.
        /// http://forums.adobe.com/thread/578130
        /// </para>
        /// </summary>
        /// <param name="photoshopInstance"></param>
        /// <param name="name"></param>
        public static void CreateNamedSnapshot(Application photoshopInstance, string name)
        {
            ActionDescriptor adesc = new ActionDescriptor();
            ActionReference aref1 = new ActionReference();
            ActionReference aref2 = new ActionReference();

            // Snapshot
            aref1.PutClass(photoshopInstance.CharIDToTypeID("SnpS"));

            // HistoryState, CurrentHistoryState
            aref2.PutProperty(
                photoshopInstance.CharIDToTypeID("HstS"),
                photoshopInstance.CharIDToTypeID("CrnH"));

            // Add the ActionReferences to the ActionDescriptor
            adesc.PutReference(photoshopInstance.CharIDToTypeID("null"), aref1);
            adesc.PutReference(photoshopInstance.CharIDToTypeID("From"), aref2);

            // Name
            adesc.PutString(photoshopInstance.CharIDToTypeID("Nm  "), name);

            // Using, HistoryState, FullDocument
            adesc.PutEnumerated(
                photoshopInstance.CharIDToTypeID("Usng"),
                photoshopInstance.CharIDToTypeID("HstS"),
                photoshopInstance.CharIDToTypeID("FllD"));

            // Make
            photoshopInstance.ExecuteAction(
                photoshopInstance.CharIDToTypeID("Mk  "),
                adesc,
                PsDialogModes.psDisplayNoDialogs);
        }

        /// <summary>
        /// Deletes a currently existing Photoshop history snapshot.
        /// </summary>
        /// <param name="photoshopInstance"></param>
        /// <param name="name"></param>
        public static void DeleteNamedSnapshot(Application photoshopInstance, string name)
        {
            ActionDescriptor adesc = new ActionDescriptor();
            ActionReference aref = new ActionReference();

            // Snapshot
            aref.PutName(photoshopInstance.CharIDToTypeID("SnpS"), name);

            // Add the ActionReference to the ActionDescriptor
            adesc.PutReference(photoshopInstance.CharIDToTypeID("null"), aref);

            // Delete
            photoshopInstance.ExecuteAction(
                photoshopInstance.CharIDToTypeID("Dlt "),
                adesc,
                PsDialogModes.psDisplayNoDialogs);
        }

        /// <summary>
        /// Reverts to a currently existing Photoshop history snapshot.
        /// </summary>
        /// <param name="photoshopInstance"></param>
        /// <param name="name"></param>
        public static void RevertToNamedSnapshot(Application photoshopInstance, string name)
        {
            ActionDescriptor adesc = new ActionDescriptor();
            ActionReference aref = new ActionReference();

            // Snapshot
            aref.PutName(photoshopInstance.CharIDToTypeID("SnpS"), name);

            // Add the ActionReference to the ActionDescriptor
            adesc.PutReference(photoshopInstance.CharIDToTypeID("null"), aref);

            // Select
            photoshopInstance.ExecuteAction(
                photoshopInstance.CharIDToTypeID("slct"),
                adesc,
                PsDialogModes.psDisplayNoDialogs);
        }
    }
}
