﻿// -----------------------------------------------------------------------
// <copyright file="PhotoshopActionHelpers.cs" company="Sector12">
//     Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Sector12Common.PhotoshopHelpers
{
    using System;
    using System.IO;
    using System.Linq;
    using Photoshop;
    using Sector12Common.Helpers;
    using Sector12Common.ResourceLoaders;

    /// <summary>
    /// <para>
    /// These photoshop methods were designed for use with Adobe Photoshop CC (2013).
    /// Many of these methods use Photoshop ScriptListeners.
    /// </para>
    /// <para>
    /// "ScriptingListener" is a plugin that used to ship with all photoshop editions.
    /// As of CS6 and above, it is now a separate download. You can get it and read 
    /// more about where to put it here:
    /// http://www.adobe.com/devnet/photoshop/scripting.html
    /// http://forums.adobe.com/thread/1237032
    /// </para>
    /// <para>
    /// The Javascript/AppleScript/VBSCript DOM cannot do everything that photoshop
    /// can. It can handle most of the basic operations, but cannot do many things
    /// like create snapshots, execute filters from the filter gallery, etc.
    /// The developers of the Photoshop DOM and interop stuff knew this, so they
    /// also created the "ScriptingListener" plugin. You can use the plugin while
    /// Photoshop is running. Every command you execute will be show as a fairly
    /// cryptic piece of code that uses Actions and Ids to execute commands in
    /// photoshop. These actions are how photoshop is actually executing operations
    /// behind the scenes, and how things get saved in Photoshop's history. 
    /// Essentially, anything you can do in photoshop that will generate a history
    /// item in the history window can be performed using ScriptingListener code.
    /// Here's a fairly outdated, but still good tutorial on how to use the plugin.
    /// Don't pay attention to the installation stuff, as the plugin is no longer
    /// shipped with Photoshop. It's a separate download.
    /// http://blogs.adobe.com/crawlspace/2006/05/installing_and_1.html
    /// </para>
    /// <para>
    /// When dealing with ScriptingListener code, there are many different character
    /// and integer codes that are passed around. A very good reference to see what
    /// these codes mean can be found in the link below. Scroll down a bit and you
    /// will see all of the mappings. I discovered this link thanks to the second
    /// link below (the forum discussion).
    /// http://ps-scripts.cvs.sourceforge.net/viewvc/ps-scripts/xtools/xlib/PSConstants.js
    /// http://forums.adobe.com/thread/578130
    /// </para>
    /// </summary>
    public static class PhotoshopActionHelpers
    {
        /// <summary>
        /// Executes a photoshop action.
        /// </summary>
        /// <param name="photoshopInstance">
        /// The current instance of photoshop.
        /// </param>
        /// <param name="actionFilePackUri">
        /// The packUri to the saved Photoshop action file (.atn). The name of the
        /// action file can be different from the name of the action set included 
        /// within the file.
        /// </param>
        /// <param name="actionSetName">
        /// The name of the Photoshop ActionSet in which our action resides.
        /// </param>
        /// <param name="actionName">
        /// The name of the Photoshop Action to execute.
        /// </param>
        public static void ExecuteAction(
            Application photoshopInstance,
            Uri actionFilePackUri,
            string actionSetName,
            string actionName)
        {
            try
            {
                // Try executing the action first. If my action set (for example, 
                // "Shaun's Custom Actions.atn") has previously been loaded into 
                // photoshop, we don't need to load it again.
                photoshopInstance.DoAction(actionName, actionSetName);
            }
            catch (Exception)
            {
                // If our first attempt failed, it's most likely because our actions
                // have not been loaded into photoshop yet. Load them now. 
                // Remember we are using an embedded resource, so use a pack uri. 
                // Photoshop cannot load action files from memory, so we have to copy 
                // it to a temporary file.
                MemoryStream stream = WpfResourceLoader.LoadResource<MemoryStream>(actionFilePackUri, ResourceLoaderContentType.MemoryStream);
                FileInfo file = FileSystemHelper.CreateTemporaryFileFromMemoryStream(stream, ".atn");

                // Load the actions into photoshop
                photoshopInstance.Load(file.FullName);

                // Try executing the action again. This time if there's an error,
                // things will blow up.
                photoshopInstance.DoAction(actionName, actionSetName);
            }
        }
    }
}
