﻿// -----------------------------------------------------------------------
// <copyright file="PhotoshopSaveHelpers.cs" company="Sector12">
//     Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Sector12Common.PhotoshopHelpers
{
    using System;
    using System.IO;
    using System.Linq;
    using Photoshop;
    using Sector12Common.Helpers;

    /// <summary>
    /// Class definition for PhotoshopSaveHelpers.cs
    /// </summary>
    public static class PhotoshopSaveHelpers
    {
        /// <summary>
        /// <para>
        /// Exports a photoshop document to file. Even if the user specifies an
        /// extension in absoluteFileNameWithoutExtension, it will be ignored.
        /// The extension will match the imageFormat specified.  
        /// If the optional "options" object specified a different format then
        /// the "imageFormat" passed in as a required parameter, the required
        /// parameter will overwrite the value specified in "options".
        /// </para>
        /// <para>
        /// Exporting a photoshop document to file is a little bit tricky 
        /// because photoshop really tries to control naming when exporting
        /// to the web. It tries to add hyphens so it will be web and unix
        /// compatible. It tries to shorten name lengths to be mac compatible. 
        /// There are a variety of settings that you can set to control this
        /// behavior. It is avaible via the fly-out dialog when exporting:
        /// File -> Save For Web -> Fly-out button -> Edit Output Settings.
        /// </para>
        /// <para>
        /// You can save your customized settings to a .iors file, and load
        /// them at a later point. However, I have not been able to figure out
        /// how to load these output settings from code. In addition, I don't
        /// really want to load and select these custom settings, because I'm
        /// not sure how to re-select the old settings the user had configured
        /// after I'm done. A simple workaround to bypass all of these photoshop
        /// naming issues is to simply have photoshop save each file as something 
        /// standard, and then rename the file to what it really should be with
        /// C#. It seems other people have had similar issues as well:
        /// http://forums.adobe.com/thread/944964
        /// </para>
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="absoluteFileNameWithoutExtension"></param>
        /// <param name="imageFormat"></param>
        /// <param name="overwrite"></param>
        /// <param name="options"></param>
        /// <returns>
        /// The final save path. This could be different from the desired file name
        /// if overwrite is not specified, and a file with the desired name already
        /// exists.
        /// </returns>
        public static string SaveForWeb(
            Document doc,
            string absoluteFileNameWithoutExtension,
            PsSaveDocumentType imageFormat,
            bool overwrite = false,
            ExportOptionsSaveForWeb options = null)
        {
            // Even if the uer specified an extension, don't use it. Photoshop will
            // save the file as whatever the imageFormat is. If the file extension
            // is different from the imageFormat, the file will be renamed. For 
            // example, if my filename is "test.png", but I've specified an 
            // imageFormat of PsSaveDocumentType.psJPEGSave, the file will be saved
            // as a JPEG, but then renamed to .png. This is a bit confusing, because
            // windows can actually handle this situation, because most photo software
            // will ignore the extension, and be able to load the file anyway. For 
            // instance, you can right-click a .png file on the windows desktop, 
            // rename it to .jpeg, and everything will work fine. The file sizes
            // stay the same however, so windows certainly isn't doing anything 
            // tricky like converting the file on the fly.
            // So, to avoid all confusion, never use the extension passed in by the 
            // user. Always have the imageFormat and the resulting extension match.
            string desiredDirectory = Path.GetDirectoryName(absoluteFileNameWithoutExtension);
            string desiredFileName = Path.GetFileNameWithoutExtension(absoluteFileNameWithoutExtension);

            // Determine the extension based on the imageFormat. Remember that only a 
            // few different save formats are supported by the "Save For Web" feature.
            string extension = DetermineSaveForWebFileExtension(imageFormat);

            // Make sure the image format specified is supported by the "Save For Web"
            // feature. If it is not, you must use "Save As...". 
            if (extension.Equals("not supported", StringComparison.CurrentCultureIgnoreCase))
                throw new Exception("The image format you specified is not supported by Photoshop's " +
                    "'Save For Web' feature. The only supported file types are .gif, .jpg, .png, " +
                    "and .wbmp.");

            // If no options were set, we'll create a default, empty options object
            if (options == null)
            {
                options = new ExportOptionsSaveForWeb();

                // Always default png saves as png-24, not png-8, so that data isn't
                // lost. If the user passes in his own options, we won't override this,
                // but we'll most likely want png-24 99% of the time. Png-8 save really
                // mess up transparency, such as the shadows around the base of trees.
                options.PNG8 = false;
            }

            // Set the image format. Even if the format was set in options, we want to
            // overwrite it with the required format we have to pass in to this method.
            options.Format = imageFormat;

            // We will first have photoshop save a file in a temporary location,
            // with a standard name.
            string tempPath = Path.GetTempPath();
            string tempFileName = tempPath + "ps_temp" + extension;

            // If the temporary file existed beforehand, for whatever reason, delete it now.
            File.Delete(tempFileName);

            // Save the file. I've actually tested this with other unsupported values,
            // such as ".tiff" and ".pcx". The ".tiff" option surprisingly worked. The ".pcx"
            // file appeared to be saved correctly, but when I tried to open it again in
            // photoshop, it said the file was corrupted. Basically, it's possible that more
            // image types could work using the Document.Export/SaveForWeb method, but I'm
            // going to limit it to what is actually available in the "Save For Web" dialog
            // from within photoshop (.gif, .jpg, .png, .wbmp).
            doc.Export(tempFileName, PsExportType.psSaveForWeb, options);

            // Now that photoshop has saved the document, we want to rename it
            // to be what we originally specified.
            string savePath = desiredDirectory + "\\" + desiredFileName + extension;

            // If we don't want to overwrite existing files, make sure we add a 
            // number to the end of the file name. Otherwise, photoshop will 
            // overwrite files automatically.
            if (!overwrite)
                savePath = FileSystemHelper.GetAlternateFileNameToAvoidOverwrite(savePath);

            // Move the file
            FileInfo fi = new FileInfo(tempFileName);
            fi.MoveTo(savePath);

            // Return the final name of the saved object
            return savePath;
        }

        /// <summary>
        /// Translates the given PsSaveDocumentType (image format) into a file
        /// extension. The only supported image types are the types that are actually 
        /// available in the "Save For Web" dialog from within photoshop (.gif, .jpg, 
        /// .png, .wbmp).
        /// </summary>
        /// <param name="imageFormat"></param>
        /// <returns>
        /// The file extension for the given format.
        /// </returns>
        public static string DetermineSaveForWebFileExtension(PsSaveDocumentType imageFormat)
        {
            switch (imageFormat)
            {
                case PsSaveDocumentType.psCompuServeGIFSave:
                    return ".gif";

                case PsSaveDocumentType.psJPEGSave:
                    return ".jpg";

                case PsSaveDocumentType.psPNGSave:
                    return ".png";

                case PsSaveDocumentType.psWirelessBitmapSave:
                    return ".wbmp";

                default:
                    return "not supported";
            }
        } 
    }
}
