﻿// -----------------------------------------------------------------------
// <copyright file="PhotoshopFontHelpers.cs" company="Sector12">
//     Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Sector12Common.PhotoshopHelpers
{
    using System;
    using System.Linq;
    using System.Windows;
    using Photoshop;

    /// <summary>
    /// Class definition for PhotoshopFontHelpers.cs
    /// </summary>
    public static class PhotoshopFontHelpers
    {
        /// <summary>
        /// Calculates the width of a space character. Space characters take up no
        /// space when drawn alone, so we need to improvise.
        /// </summary>
        /// <param name="textLayerToCopy"></param>
        /// <returns>
        /// The width of a space character.
        /// </returns>
        public static double CalculateSpaceCharacterWidth(ArtLayer textLayerToCopy)
        {
            // First, calculate the bounds of two letters side by side. I chose "AA" arbitrarily.
            // Next, calculate the width of the same two letters with a space between them.
            Rect r1 = CalculateStringBoundsRect(textLayerToCopy, "AA");
            Rect r2 = CalculateStringBoundsRect(textLayerToCopy, "A A");

            // Return the difference between the two widths
            return r2.Width - r1.Width;
        }

        /// <summary>
        /// Draws the string or character in photoshop and returns its bounds.
        /// </summary>
        /// <param name="textLayerToCopy"></param>
        /// <param name="s"></param>
        /// <returns>
        /// The bounds of the string, using the font settings specified in the 
        /// given text layer.
        /// </returns>
        public static Rect CalculateStringBoundsRect(ArtLayer textLayerToCopy, string s)
        {
            // Create a copy of our text layer
            ArtLayer copy = textLayerToCopy.Duplicate();

            // Paste the string into our copy layer
            copy.TextItem.Contents = s;

            // Convert the new layer's bounds to a more usable rect object
            Rect r = PhotoshopDrawingHelpers.ConvertPhotoshopBoundsToRect(copy.Bounds);

            // Destroy our temporary copy
            copy.Delete();

            // Return the bounding rectangle
            return r;
        }
    }
}
