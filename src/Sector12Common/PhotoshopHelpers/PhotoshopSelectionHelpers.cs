﻿// -----------------------------------------------------------------------
// <copyright file="PhotoshopSelectionHelpers.cs" company="Sector12">
//     Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Sector12Common.PhotoshopHelpers
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using Photoshop;

    /// <summary>
    /// Class definition for PhotoshopSelectionHelpers.cs
    /// </summary>
    public static class PhotoshopSelectionHelpers
    {
        /// <summary>
        /// Load's the specified layer's transparency channel as a selection. See the
        /// following discussion or more details:
        /// http://www.ps-scripts.com/bb/viewtopic.php?f=2&t=3602
        /// </summary>
        /// <param name="photoshopInstance"></param>
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1603:DocumentationMustContainValidXml", Justification = "Reviewed. Suppression is OK here.")]
        public static void SelectActiveLayerContents(Application photoshopInstance)
        {
            ActionDescriptor adesc = new ActionDescriptor();
            ActionReference aref = new ActionReference();
            ActionReference aref2 = new ActionReference();

            // Select channel
            aref.PutProperty(
                photoshopInstance.CharIDToTypeID("Chnl"),
                photoshopInstance.CharIDToTypeID("fsel"));

            // Type of channel we are selecting
            aref2.PutEnumerated(
                photoshopInstance.CharIDToTypeID("Chnl"),
                photoshopInstance.CharIDToTypeID("Chnl"),
                photoshopInstance.CharIDToTypeID("Trsp"));

            // Add the ActionReferences to the ActionDescriptor
            adesc.PutReference(photoshopInstance.CharIDToTypeID("null"), aref);
            adesc.PutReference(photoshopInstance.CharIDToTypeID("T   "), aref2);

            // Execute the action
            photoshopInstance.ExecuteAction(
                photoshopInstance.CharIDToTypeID("setd"),
                adesc,
                PsDialogModes.psDisplayNoDialogs);
        }
    }
}
