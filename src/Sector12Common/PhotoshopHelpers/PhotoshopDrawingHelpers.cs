﻿// -----------------------------------------------------------------------
// <copyright file="PhotoshopDrawingHelpers.cs" company="Sector12">
//     Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Sector12Common.PhotoshopHelpers
{
    using System;
    using System.Linq;
    using System.Windows;
    using Photoshop;

    /// <summary>
    /// Class definition for PhotoshopDrawingHelpers.cs
    /// </summary>
    public static class PhotoshopDrawingHelpers
    {
        /// <summary>
        /// Photoshop bounds are built slightly differently than a typical
        /// rectangle that you would see in something like XNA. This translates
        /// the photoshop bounds to something we are more comfortable with.
        /// A photoshop bounds object will always have exactly 4 elements.
        /// </summary>
        /// <param name="bounds"></param>
        /// <returns>
        /// The bounds, as a rect.
        /// </returns>
        public static Rect ConvertPhotoshopBoundsToRect(object[] bounds)
        {
            double left = (double)bounds[0]; // bounds[0] == top left corner x
            double top = (double)bounds[1]; // bounds[1] == top left corner y
            double right = (double)bounds[2]; // bounds[2] == bottom right corner x
            double bottom = (double)bounds[3]; // bounds[2] == bottom right corner y

            double width = right - left;
            double height = bottom - top;

            return new Rect(left, top, width, height);
        }

        /// <summary>
        /// Performs a "Reveal All" operation and fills the background with
        /// the specified background color.
        /// </summary>
        /// <param name="psDocument"></param>
        /// <param name="backgroundLayer"></param>
        /// <param name="backgroundFillColor"></param>
        public static void RevealAndFill(
            Document psDocument, 
            ArtLayer backgroundLayer,
            SolidColor backgroundFillColor)
        {
            // Expand the canvas to fit everything in our document
            psDocument.RevealAll();

            // Fill the background layer. Note that this must be done
            // even if PsDocumentFill is set to PsDocumentFill.psBackgroundColor.
            // When set to psBackgroundColor, expanding the canvas manually will 
            // maintain the background color fill in the new areas, but the color
            // will always be white when calling RevealAll(). You can test this
            // in photoshop by creating a new document with a locked, colored
            // background, adding text that goes beyond the borders of the canvas, 
            // and then clicking Image -> Reveal All.
            psDocument.Selection.SelectAll();
            psDocument.ActiveLayer = backgroundLayer;
            psDocument.Selection.Fill(backgroundFillColor);

            // Deselect everything
            psDocument.Selection.Deselect();
        }

        /// <summary>
        /// Resizes the canvas to fit the layer content.
        /// </summary>
        /// <param name="psDocument"></param>
        public static void CropToSize(Document psDocument)
        {
            // Trim will crop the canvas to fit the layer content
            psDocument.Trim();
        }

        /// <summary>
        /// Resizes the canvas to fit the layer content, plus the specified
        /// buffer size. The buffer is added to all sides of the canvas.
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="psDocument"></param>
        /// <param name="backgroundLayer"></param>
        /// <param name="backgroundFillColor"></param>
        public static void CropToSize(
            int buffer, 
            Document psDocument, 
            ArtLayer backgroundLayer, 
            SolidColor backgroundFillColor)
        {
            // Trim will crop the canvas to fit the layer content
            psDocument.Trim();

            // Add the specified buffer to all sides
            psDocument.ResizeCanvas(
                psDocument.Width + (buffer * 2),
                psDocument.Height + (buffer * 2),
                PsAnchorPosition.psMiddleCenter);

            // Fill the newly expanded background layer
            psDocument.Selection.SelectAll();
            psDocument.ActiveLayer = backgroundLayer;
            psDocument.Selection.Fill(backgroundFillColor);

            // Deselect everything
            psDocument.Selection.Deselect();
        }

        /// <summary>
        /// Centers a layer within the center of the given document.
        /// </summary>
        /// <param name="layer"></param>
        /// <param name="document"></param>
        public static void CenterInCanvas(ArtLayer layer, Document document)
        {
            // Determine the bounds of the layer we are  centering
            Rect layerBounds = PhotoshopDrawingHelpers.ConvertPhotoshopBoundsToRect(layer.Bounds);

            // Determine the center coordinates
            double x = (document.Width / 2) - (layerBounds.Width / 2);
            double y = (document.Height / 2) - (layerBounds.Height / 2);

            // Move the layer
            MoveLayerToAbsolutePosition(layer, (int)x, (int)y);
        }

        /// <summary>
        /// Moves the art layer to an absolute position on the canvas. Remember, 
        /// moving layers in photoshop can only be done via the translate() function, 
        /// which uses relative, not absolute positioning.
        /// http://stackoverflow.com/questions/12064015/photoshop-scripting-move-an-image-to-position-x-y
        /// </summary>
        /// <param name="layer"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public static void MoveLayerToAbsolutePosition(ArtLayer layer, int x, int y)
        {
            // First, get the current bounds of the layer
            Rect pos = PhotoshopDrawingHelpers.ConvertPhotoshopBoundsToRect(layer.Bounds);

            // Desired position minus current position
            int xdiff = x - (int)pos.Left;
            int ydiff = y - (int)pos.Top;

            // Move the layer the required distance it needs to go
            // in order to be in the final, desired location.
            layer.Translate(xdiff, ydiff);
        }
    }
}
