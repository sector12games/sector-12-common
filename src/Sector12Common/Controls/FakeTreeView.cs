﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FakeTreeView.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Sector12Common.Controls
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using Sector12Common.Helpers;

    /// <summary>
    /// <para>
    /// A custom "fake" tree control. This control is actually a ListView,
    /// but uses some indention tricks to display like a tree.
    /// </para>
    /// <para>
    /// At the time of writing (2010), no TreeView control existed in WPF.
    /// </para>
    /// </summary>
    [TemplatePart(Name = "PART_listView", Type = typeof(ListView))]
    public class FakeTreeView : ContentControl
    {
        public static readonly DependencyProperty ItemsSourceProperty =
           DependencyProperty.Register(
               "ItemsSource",
               typeof(IEnumerable<FakeTreeViewItem>),
               typeof(FakeTreeView),
               new PropertyMetadata(null, ItemsSource_ValueChanged));

        private FakeTreeViewItemController _controller;
        private ListView _listView;

        /// <summary>
        /// Initializes static members of the <see cref="FakeTreeView"/> class.
        /// </summary>
        static FakeTreeView()
        {
            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(FakeTreeView), 
                new FrameworkPropertyMetadata(typeof(FakeTreeView)));
        }

        /// <summary>
        /// Gets or sets the ItemsSource. This is our backing node list.
        /// </summary>
        [Bindable(true)]
        public IEnumerable<FakeTreeViewItem> ItemsSource
        {
            get
            {
                return (IEnumerable<FakeTreeViewItem>)GetValue(ItemsSourceProperty);
            }

            set
            {
                SetValue(ItemsSourceProperty, value);
            }
        }

        /// <summary>
        /// Event listener for when the ItemsSource value changes.
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        public static void ItemsSource_ValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            FakeTreeView fakeTreeView = d as FakeTreeView;
            fakeTreeView.UpdateAfterItemsSourceValueChanged(e.OldValue);
        }

        /// <summary>
        /// Safe place to add hookups.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            // Add event handlers
            Loaded += FakeTreeView_Loaded;
            AddHandler(Keyboard.KeyDownEvent, new KeyEventHandler(FakeTreeView_KeyDown), true);

            // get a reference to the listview
            _listView = GetTemplateChild("PART_listView") as ListView;
        }

        /// <summary>
        /// Event listener for keyboard input. Allows the tree to be navigated
        /// via the keyboard.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void FakeTreeView_KeyDown(object sender, KeyEventArgs e)
        {
            // Get the selected item
            FakeTreeViewItem selectedItem = _listView.SelectedItem as FakeTreeViewItem;
            ListViewItem selectedGraphicalItem = 
                _listView.ItemContainerGenerator.ContainerFromItem(selectedItem) as ListViewItem;

            // A little bit of custom logic for checkbox children. It feels kind
            // of dirty doing it this way, but it works. If space bar is pressed,
            // check or uncheck the currently selected item.
            if (e.Key == Key.Space)
            {
                // Find the nearest checkbox child
                CheckBox child = WpfHelpers.TryFindChild<CheckBox>(selectedGraphicalItem);

                // Don't check the box if the checkbox actually has focus.
                // If the checkbox has focus and you press spacebar, the checkbox
                // will use it's default checkbox logic to check it already. We 
                // don't want to perform the same logic twice. Only check the box
                // if the FakeTreeView's row is selected, but the checkbox is not.
                if (child != null && !child.IsFocused)
                    child.IsChecked = !child.IsChecked;

                return;
            }

            if (e.Key == Key.Left)
            {
                FakeTreeViewItem parent = FindParentByTreeLevel(selectedItem);
                
                // Collapse the item if it isn't already collapsed
                if (selectedItem.IsExpanded && selectedItem.HasChildren)
                {
                    selectedItem.IsExpanded = false;
                }
                else if (parent != null)
                {
                    // If the selected item is already collapsed, collapse
                    // the parent, if one exists. Make sure to select the
                    // parent after we collpase it.
                    parent.IsExpanded = false;
                    _listView.SelectedItem = parent;
                }
                else
                {
                    // Otherwise, just move up to the next item in the list
                    _listView.SelectedIndex--;
                }
            }

            if (e.Key == Key.Right)
            {
                // Expand the item if it isn't already expanded. Otherwise, just 
                // move down to the next item in the list.
                if (!selectedItem.IsExpanded && selectedItem.HasChildren)
                    selectedItem.IsExpanded = true;
                else
                    _listView.SelectedIndex++;
            }

            // Make sure we never get -1. -1 means nothing is selected. The listview
            // already has logic built in to not allow values greater than the size
            // of the collection.
            if (_listView.SelectedIndex < 0)
                _listView.SelectedIndex = 0;

            // Whatever happened with the right/left expanding/collapsing,
            // make sure that the selected item actually has focus as well.
            // I'm not sure wtf WPF is thinking, but the focus rect is 
            // separate from the selection background. So the rect  will
            // be on a row, while a different row will be highlighted blue.
            selectedGraphicalItem = _listView.ItemContainerGenerator.ContainerFromItem(
                _listView.SelectedItem) as ListViewItem;

            if (selectedGraphicalItem != null)
                selectedGraphicalItem.Focus();
        }

        /// <summary>
        /// Rebuilds the tree when the backing collection changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        // ReSharper disable UnusedParameter.Global
        public void ItemsSource_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            BuildFakeTreeView();
        }
        // ReSharper restore UnusedParameter.Global

        /// <summary>
        /// Event listener for when the tree view is fully loaded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void FakeTreeView_Loaded(object sender, RoutedEventArgs e)
        {
            // Read my comment in UpdateAfterItemsSourceValueChanged to see why
            // it is necessary to call this method when the control is loaded
            UpdateAfterItemsSourceValueChanged(null);
        }

        /// <summary>
        /// Called every time the ItemsSource value changes. This will hook
        /// up some event listeners to the backing collection, and build the
        /// tree.
        /// </summary>
        /// <param name="oldItemsSource"></param>
        private void UpdateAfterItemsSourceValueChanged(object oldItemsSource)
        {
            // We are not guaranteed that OnApplyTemplate will always have been
            // called before we reach this point.  It's possible the ItemsSource
            // was set explicitly in the constructor of the window that owns 
            // this transfer control, and it appears to be sort of random whether
            // or not this callback will be executed before or after OnApplyTemplate
            // is called.  Thus, our listview could be null.  To remedy
            // this, we'll make sure this method is called whenever the 
            // FakeTreeView's Loaded event is fired as well.
            if (_listView == null)
                return;

            // If the ItemsSource was set to null, just set the
            // ItemsSource of our listview to null
            if (ItemsSource == null)
            {
                _listView.ItemsSource = null;
                return;
            }

            // If our ItemsSource happens to be an ObservableCollection,
            // we want to listen to the CollectionChanged event as well.
            // We need to rebuild our fake tree view every time the
            // ObservableCollection's CollectionChanged event fires.
            // 
            // We have to use some reflection magic here, because of a few
            // problems. First of all, we can't upcast an ObservableCollection<T>
            // to ObservableCollection<FakeTreeView>. For example, if I try
            // to do this, "temp" will be null:
            // ObservableCollection<ZoneDataModel> zones;
            // ObservableCollection<FakeTreeViewItem> temp = zones as ObservableCollection<FakeTreeViewItem>
            //
            // A potential workaround is to make this entire FakeTreeViewClass generic:
            // public class (FakeTreeView<T> : Control where T : FakeTreeViewItem
            // However, this won't work as XAML does not support generics as
            // of .NET 4.0. 
            //
            // So, we're left with no other option but to use reflection magic to 
            // tie the ObservableCollection's CollectionChanged event to this
            // class. Here's where I got the inspiration for this solution:
            // http://stackoverflow.com/questions/1121441/c-sharp-addeventhandler-using-reflection
            //
            // Note that if the ItemsSource object is NOT an observable collection,
            // but maybe a List<>, this piece of functionality will simply fail
            // silently and the program will not be affected.
            try
            {
                // Add the CollectionChanged event handler to our ObservableCollection object.
                var target = ItemsSource;
                var eventInfo = target.GetType().GetEvent("CollectionChanged");
                var methodInfo = GetType().GetMethod("ItemsSource_CollectionChanged");
                Delegate handler = Delegate.CreateDelegate(eventInfo.EventHandlerType, this, methodInfo);
                eventInfo.AddEventHandler(target, handler);

                // Remove the event handler from the oldItemsSource when the
                // ItemsSource changes. If we don't do this, the old ObservableCollection<> 
                // will still have the handler attached, and any modifications to it
                // will call the handler and cause our list to be rebuilt. I have thoroughly
                // tested this by:
                // - Setting an ItemsSource
                // - Changing the ItemsSource, but saving the old one
                // - Modifying the old ItemsSource, and seeing what happens
                // This does in fact successfully remove the event handler, and all works
                // as expected.
                eventInfo.RemoveEventHandler(oldItemsSource, handler);
            }
            // ReSharper disable once EmptyGeneralCatchClause
            catch (Exception)
            {
                // Fail silently if the ItemsSource is not an observable collection
            }

            // Build the fake treeview
            BuildFakeTreeView();
        }

        /// <summary>
        /// Build the tree.
        /// </summary>
        private void BuildFakeTreeView()
        {
            // We're going to fake a treeview by using a custom ListView,
            // with a controller that adds/removes items from the list
            // when items are expanded/collapsed
            _controller = new FakeTreeViewItemController(ItemsSource, _listView);
            _listView.ItemsSource = _controller.AllListViewItems;
        }

        /// <summary>
        /// Gets the specified child's index within the list.
        /// </summary>
        /// <param name="child"></param>
        /// <returns>The child's index.</returns>
        private int GetListViewIndex(FakeTreeViewItem child)
        {
            for (int i = 0; i < _listView.Items.Count; i++)
            {
                if (_listView.Items[i] == child)
                    return i;
            }

            // The child was not found in the list
            return -1;
        }

        /// <summary>
        /// Navigates backwards in the list and finds the first item that
        /// has a smaller tree depth than the specified child.
        /// </summary>
        /// <param name="child"></param>
        /// <returns>
        /// The child's parent.
        /// </returns>
        private FakeTreeViewItem FindParentByTreeLevel(FakeTreeViewItem child)
        {
            int index = GetListViewIndex(child);

            for (int i = index; i >= 0; i--)
            {
                FakeTreeViewItem item = _listView.Items[i] as FakeTreeViewItem;
                if (item.TreeLevel < child.TreeLevel)
                    return item;
            }

            return null;
        }
    }
}
