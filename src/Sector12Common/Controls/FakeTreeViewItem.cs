﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FakeTreeViewItem.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Sector12Common.Controls
{
    using System;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Linq;

    /// <summary>
    /// Class definition for FakeTreeViewItem.cs.
    /// </summary>
    public abstract class FakeTreeViewItem : INotifyPropertyChanged
    {
        private readonly ObservableCollection<FakeTreeViewItem> _children;

        private bool _isExpanded;
        private bool _childrenHaveBeenLoaded;

        /// <summary>
        /// Initializes a new instance of the <see cref="FakeTreeViewItem"/> class.
        /// </summary>
        protected FakeTreeViewItem()
        {
            _children = new ObservableCollection<FakeTreeViewItem>();
        }

        /// <summary>
        /// EventHandler for the property changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets this node's depth within the tree.
        /// </summary>
        public abstract int TreeLevel { get; }

        /// <summary>
        /// Gets a value indicating whether or not this node has children.
        /// </summary>
        public abstract bool HasChildren { get; }

        /// <summary>
        /// Gets the list of this node's childen.
        /// </summary>
        public ObservableCollection<FakeTreeViewItem> Children
        {
            get
            {
                return _children;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not this node is visually
        /// expanded within the tree view.
        /// </summary>
        public bool IsExpanded
        {
            get
            {
                return _isExpanded;
            }

            set
            {
                if (_isExpanded == value)
                    return;

                _isExpanded = value;

                if (value && !_childrenHaveBeenLoaded)
                {
                    // Load the children
                    _childrenHaveBeenLoaded = true;
                    LoadChildren();
                }

                OnPropertyChanged("IsExpanded");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not this node's children
        /// have already been loaded.
        /// </summary>
        public bool ChildrenHaveBeenLoaded
        {
            get
            {
                return _childrenHaveBeenLoaded;
            }

            set
            {
                _childrenHaveBeenLoaded = value;
            }
        }

        /// <summary>
        /// Gets or sets this node's parent.
        /// </summary>
        public FakeTreeViewItem Parent { get; set; }

        /// <summary>
        /// Loads all of this node's child items.
        /// </summary>
        public abstract void LoadChildren();

        /// <summary>
        /// OnPropertyChanged method helper.
        /// </summary>
        /// <param name="propertyName"></param>
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
