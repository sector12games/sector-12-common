﻿// -----------------------------------------------------------------------
// <copyright file="ProgressBarTask.cs" company="Sector12">
//     Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Sector12Common.Controls.ControlData
{
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Windows;

    /// <summary>
    /// This class is for use within the DualProgressBars control.
    /// </summary>
    [SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1201:ElementsMustAppearInTheCorrectOrder",
        Justification = "Reviewed. Dependency Properties are ordered differently.")]
    public class ProgressBarTask : WorkerThreadSafeContentControl
    {
        #region Fields, Constructors, Delegates

        /// <summary>
        /// Initializes static members of the <see cref="ProgressBarTask"/> class. 
        /// This static constructor is required for WPF custom controls, to ensure
        /// it is using the correct styles. You can add additional, non-static 
        /// constructors if you like.
        /// </summary>
        static ProgressBarTask()
        {
            // Standard code for creating a WPF custom control.
            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(ProgressBarTask),
                new FrameworkPropertyMetadata(typeof(ProgressBarTask)));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProgressBarTask"/> class.
        /// </summary>
        public ProgressBarTask()
        {
            Subtasks = new ObservableCollection<ProgressBarSubtask>();
        }

        /// <summary>
        /// <para>
        /// Initializes a new instance of the <see cref="ProgressBarTask"/> class. 
        /// </para>
        /// <para>
        /// By opting to use this constructor, you are choosing not to
        /// initialize all of your subtasks at this point. New ProgressBarSubtask
        /// objects will be created for each subtaskCount, but they will all have
        /// empty strings for their TaskName and TaskDetails. In order to set these
        /// values, you must use the method from your code:
        /// DualProgressBars.NextTask(subtaskName, subtaskDetails)
        /// </para>
        /// </summary>
        /// <param name="taskName"></param>
        /// <param name="taskDetails"></param>
        /// <param name="subtaskCount"></param>
        public ProgressBarTask(string taskName, string taskDetails, int subtaskCount)
        {
            TaskName = taskName;
            TaskDetails = taskDetails;

            // Initialize an observable collection to hold our subtasks. The name and
            // details have not yet been set on these subtasks, so it is assumed that
            // the user will use the "NextTask(subtaskName, subtaskDetails)" method
            // to set the subtask values as he goes, instead of initializing all subtasks
            // before the progress bar starts running.
            ObservableCollection<ProgressBarSubtask> c = new ObservableCollection<ProgressBarSubtask>();
            for (int i = 0; i < subtaskCount; i++)
            {
                c.Add(new ProgressBarSubtask());
            }

            Subtasks = c;
        }

        /// <summary>
        /// <para>
        /// Initializes a new instance of the <see cref="ProgressBarTask"/> class. 
        /// </para>
        /// <para>
        /// By opting to use this constructor, you are choosing to initialize all
        /// of your subtasks before the progress bar even begins running. Because
        /// all of your subtasks are initialized already, you can call:
        /// DualProgressBars.NextTask() from your code instead of:
        /// DualProgressBars.NextTask(subtaskName, subtaskDetails)
        /// </para>
        /// </summary>
        /// <param name="taskName"></param>
        /// <param name="taskDetails"></param>
        /// <param name="subtasks"></param>
        public ProgressBarTask(string taskName, string taskDetails, ObservableCollection<ProgressBarSubtask> subtasks)
        {
            TaskName = taskName;
            TaskDetails = taskDetails;
            Subtasks = subtasks;
        }

        #endregion

        #region Dependency Properties

        /// <summary>
        /// Gets or sets the name of this task. This text will appear 
        /// directly above the task progress bar.
        /// </summary>
        public string TaskName
        {
            get
            {
                return (string)SafelyGetValue(TaskNameProperty);
            }

            set
            {
                SafelySetValue(TaskNameProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets additional detail about this primary task.
        /// This is essentially a way to show the user what you are doing
        /// to complete the current task. This text will appear
        /// directly to the right of the primary task progress bar.
        /// </summary>
        public string TaskDetails
        {
            get
            {
                return (string)SafelyGetValue(TaskDetailsProperty);
            }

            set
            {
                SafelySetValue(TaskDetailsProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not this task, and all of its
        /// subtasks are complete.
        /// </summary>
        public bool IsComplete
        {
            get
            {
                return (bool)SafelyGetValue(IsCompleteProperty);
            }

            set
            {
                SafelySetValue(IsCompleteProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the list of all subtasks.
        /// </summary>
        public ObservableCollection<ProgressBarSubtask> Subtasks
        {
            get
            {
                return (ObservableCollection<ProgressBarSubtask>)SafelyGetValue(SubtasksProperty);
            }

            set
            {
                SafelySetValue(SubtasksProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the total number of subtasks completed.
        /// </summary>
        public double SubtasksCompleted
        {
            get
            {
                return (double)SafelyGetValue(SubtasksCompletedProperty);
            }

            set
            {
                SafelySetValue(SubtasksCompletedProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not our current primary task 
        /// has a subtask. This is a read-only property. For more detail about 
        /// read-only dependency properties, see my comments above the 
        /// "HasSubtaskProperty" declaration.
        /// </summary>
        public bool HasSubtask
        {
            get
            {
                return (bool)SafelyGetValue(HasSubtaskProperty);
            }

            protected set
            {
                // Notice that we are setting the value on the property
                // "Key", not the property itself.
                // Also notice the protected setter. This is for read only.
                // Also call "RegisterReadOnly" instead of "Register"
                // when registering this dependency property.
                SafelySetValue(HasSubtaskPropertyKey, value);
            }
        }

        #endregion

        #region Dependency Property Registers

        /// <summary>
        /// Dependency property for TaskName.
        /// </summary>
        public static readonly DependencyProperty TaskNameProperty = DependencyProperty.Register(
            "TaskName",
            typeof(string),
            typeof(ProgressBarTask),
            new FrameworkPropertyMetadata("Default Task Name..."));

        /// <summary>
        /// Dependency property for TaskDetails.
        /// </summary>
        public static readonly DependencyProperty TaskDetailsProperty = DependencyProperty.Register(
            "TaskDetails",
            typeof(string),
            typeof(ProgressBarTask),
            new FrameworkPropertyMetadata("Default Task Details"));

        /// <summary>
        /// Dependency property for IsComplete.
        /// </summary>
        public static readonly DependencyProperty IsCompleteProperty = DependencyProperty.Register(
            "IsComplete",
            typeof(bool),
            typeof(ProgressBarTask),
            new FrameworkPropertyMetadata(false));

        /// <summary>
        /// Dependency property for SubtasksCompleted.
        /// </summary>
        public static readonly DependencyProperty SubtasksCompletedProperty = DependencyProperty.Register(
            "SubtasksCompleted",
            typeof(double),
            typeof(ProgressBarTask),
            new FrameworkPropertyMetadata(0.0));

        /// <summary>
        /// Dependency property for Subtasks.
        /// </summary>
        public static readonly DependencyProperty SubtasksProperty = DependencyProperty.Register(
            "Subtasks",
            typeof(ObservableCollection<ProgressBarSubtask>),
            typeof(ProgressBarTask),
            new FrameworkPropertyMetadata(
                new ObservableCollection<ProgressBarSubtask>(),
                SubtasksProperty_ValueChanged));

        #endregion

        #region Read-Only Dependency Property Registers

        /// <summary>
        /// Read-only dependency property key for HasSubtaskProperty.
        /// </summary>
        public static readonly DependencyPropertyKey HasSubtaskPropertyKey = DependencyProperty.RegisterReadOnly(
            "HasSubtask",
            typeof(bool),
            typeof(ProgressBarTask),
            new FrameworkPropertyMetadata(false));

        /// <summary>
        /// <para>
        /// Read-only dependency property for HasSubtask.
        /// </para>
        /// <para>
        /// This must have a matching DependencyPropertyKey, and be declared BELOW
        /// the key in code. 
        /// </para>
        /// </summary>
        public static readonly DependencyProperty HasSubtaskProperty = HasSubtaskPropertyKey.DependencyProperty;

        #endregion

        #region Event Handlers

        /// <summary>
        /// Remember that this callback method will only be called when the property
        /// itself changes (the collection is set to null, or a different collection).
        /// It will not be called when items are added to or removed from the list.
        /// Thus, we can't simply update HasSubtask in this callback method. Instead,
        /// we need to make sure that each collection is wired up with a 
        /// CollectionChanged event handler. Check out the following discussion for
        /// more details:
        /// http://stackoverflow.com/questions/12745060/dependency-properties-propertychangedcallback-not-getting-called
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void SubtasksProperty_ValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            // Get the old and new values
            ProgressBarTask task = (ProgressBarTask)d;
            INotifyCollectionChanged oldCollection = e.OldValue as INotifyCollectionChanged;
            INotifyCollectionChanged newCollection = e.NewValue as INotifyCollectionChanged;

            // Remove the event handler from the old collection
            if (oldCollection != null)
                oldCollection.CollectionChanged -= task.Subtasks_CollectionChanged;

            // Add the event handler to the new collection
            if (newCollection != null)
                newCollection.CollectionChanged += task.Subtasks_CollectionChanged;

            // Also make sure that we update our HasSubtask value here as well.
            // It's possible the collection will never change after it is initialized.
            task.HasSubtask = task.Subtasks.Any();
        }

        /// <summary>
        /// CollectionChanged event handler for our Subtasks list.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Subtasks_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            // Update the HasSubtask property any time the collection is modified
            HasSubtask = Subtasks.Any();
        }

        #endregion
    }
}