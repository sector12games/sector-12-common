﻿// -----------------------------------------------------------------------
// <copyright file="ProgressBarSubtask.cs" company="Sector12">
//     Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Sector12Common.Controls.ControlData
{
    using System.Diagnostics.CodeAnalysis;
    using System.Windows;

    /// <summary>
    /// <para>
    /// A custom control for displaying progress bars for both a primary and
    /// a secondary task. While developing this control, I developed some
    /// general rules that I'd like to follow for future controls.
    /// </para>
    /// <para>
    /// Rules to follow for future custom control development:
    ///   - Try to use only {TemplateBinding}, and NOT {Binding} in Generic.xaml. 
    ///     Template binding is checked at compile time, and can help prevent
    ///     potential binding errors in the future. Use {Binding} in regular apps,
    ///     use {TemplateBinding} in control templates.
    ///   - Because we're using only template binding, don't have custom controls
    ///     implement INotifyPropertyChanged. They can if they REALLY need to, but
    ///     in general, use read-only dependency properties instead.
    ///   - Always be very mindful of your types and potential type conversion 
    ///     when using {TemplateBinding}. Template binding does not support auto
    ///     conversion from double to int, string to int, etc. Read my comments
    ///     in Generic.xaml (DualProgressBars) for more info.
    ///   - DependencyProperty default values are used when viewing the control
    ///     in the WPF designer. The CLR getters are not called, and the constructor
    ///     is not run until runtime. Make sure to use default values that will be
    ///     helpful in the designer.
    /// </para>
    /// </summary>
    [SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1201:ElementsMustAppearInTheCorrectOrder",
        Justification = "Reviewed. Dependency Properties are ordered differently.")]
    public class ProgressBarSubtask : WorkerThreadSafeContentControl
    {
        /// <summary>
        /// Dependency property for TaskName.
        /// </summary>
        public static readonly DependencyProperty TaskNameProperty = DependencyProperty.Register(
            "TaskName",
            typeof(string),
            typeof(ProgressBarSubtask),
            new FrameworkPropertyMetadata("Default Subtask Name..."));

        /// <summary>
        /// Dependency property for TaskDetails.
        /// </summary>
        public static readonly DependencyProperty TaskDetailsProperty = DependencyProperty.Register(
            "TaskDetails",
            typeof(string),
            typeof(ProgressBarSubtask),
            new FrameworkPropertyMetadata("Default Subtask Details"));

        /// <summary>
        /// Dependency property for IsComplete.
        /// </summary>
        public static readonly DependencyProperty IsCompleteProperty = DependencyProperty.Register(
            "IsComplete",
            typeof(bool),
            typeof(ProgressBarSubtask),
            new FrameworkPropertyMetadata(false));

        /// <summary>
        /// Initializes static members of the <see cref="ProgressBarSubtask"/> class. 
        /// This static constructor is required for WPF custom controls, to ensure
        /// it is using the correct styles. You can add additional, non-static 
        /// constructors if you like.
        /// </summary>
        static ProgressBarSubtask()
        {
            // Standard code for creating a WPF custom control.
            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(ProgressBarSubtask),
                new FrameworkPropertyMetadata(typeof(ProgressBarSubtask)));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProgressBarSubtask"/> class.
        /// </summary>
        public ProgressBarSubtask()
        {
            // Required for WPF
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProgressBarSubtask"/> class.
        /// </summary>
        /// <param name="taskName"></param>
        /// <param name="taskDetails"></param>
        public ProgressBarSubtask(string taskName, string taskDetails)
        {
            TaskName = taskName;
            TaskDetails = taskDetails;
        }

        /// <summary>
        /// Gets or sets the name of this task. This text will appear 
        /// directly above the task progress bar.
        /// </summary>
        public string TaskName
        {
            get
            {
                return (string)SafelyGetValue(TaskNameProperty);
            }

            set
            {
                SafelySetValue(TaskNameProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets additional detail about this primary task.
        /// This is essentially a way to show the user what you are doing
        /// to complete the current task. This text will appear
        /// directly to the right of the primary task progress bar.
        /// </summary>
        public string TaskDetails
        {
            get
            {
                return (string)SafelyGetValue(TaskDetailsProperty);
            }

            set
            {
                SafelySetValue(TaskDetailsProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not this subtask is complete.
        /// </summary>
        public bool IsComplete
        {
            get
            {
                return (bool)SafelyGetValue(IsCompleteProperty);
            }

            set
            {
                SafelySetValue(IsCompleteProperty, value);
            }
        }
    }
}