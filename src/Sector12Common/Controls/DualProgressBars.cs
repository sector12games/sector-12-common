﻿// -----------------------------------------------------------------------
// <copyright file="DualProgressBars.cs" company="Sector12">
//     Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Sector12Common.Controls
{
    using System;
    using System.Collections.ObjectModel;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Windows;
    using Sector12Common.Controls.ControlData;

    /// <summary>
    /// <para>
    /// A custom control for displaying progress bars for both a primary and
    /// a secondary task. While developing this control, I developed some
    /// general rules that I'd like to follow for future controls.
    /// </para>
    /// <para>
    /// Rules to follow for future custom control development:
    ///   - Try to use only {TemplateBinding}, and NOT {Binding} in Generic.xaml. 
    ///     Template binding is checked at compile time, and can help prevent
    ///     potential binding errors in the future. Use {Binding} in regular apps,
    ///     use {TemplateBinding} in control templates.
    ///   - Because we're using only template binding, don't have custom controls
    ///     implement INotifyPropertyChanged. They can if they REALLY need to, but
    ///     in general, use read-only dependency properties instead.
    ///   - Always be very mindful of your types and potential type conversion 
    ///     when using {TemplateBinding}. Template binding does not support auto
    ///     conversion from double to int, string to int, etc. Read my comments
    ///     in Generic.xaml (DualProgressBars) for more info.
    ///   - DependencyProperty default values are used when viewing the control
    ///     in the WPF designer. The CLR getters are not called, and the constructor
    ///     is not run until runtime. Make sure to use default values that will be
    ///     helpful in the designer.
    /// </para>
    /// </summary>
    [SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1202:ElementsMustBeOrderedByAccess",
        Justification = "Reviewed. Dependency Properties are ordered differently.")]
    public class DualProgressBars : WorkerThreadSafeContentControl
    {
        #region Fields, Constructors, Delegates

        private const double DefaultProgressBarWidth = 300.0;

        /// <summary>
        /// Initializes static members of the <see cref="DualProgressBars"/> class. 
        /// This static constructor is required for WPF custom controls, to ensure
        /// it is using the correct styles. You can add additional, non-static 
        /// constructors if you like.
        /// </summary>
        static DualProgressBars()
        {
            // Standard code for creating a WPF custom control.
            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(DualProgressBars),
                new FrameworkPropertyMetadata(typeof(DualProgressBars)));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DualProgressBars"/> class. 
        /// </summary>
        public DualProgressBars()
        {
            Reset(true);
        }

        #endregion

        #region Dependency Properties

        /// <summary>
        /// Gets or sets the width of both the primary and subtask progress bars. 
        /// The type of this value must be "double" because of how we use 
        /// {TemplateBinding} in our Generic.xaml control template. Always be 
        /// very mindful of your types and potential type conversion when using 
        /// {TemplateBinding}. Please see my detailed comments in Generic.xaml 
        /// for more info. 
        /// </summary>
        public double ProgressBarWidth
        {
            get
            {
                return (double)SafelyGetValue(ProgressBarWidthProperty);
            }

            set
            {
                SafelySetValue(ProgressBarWidthProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the current primary task. 
        /// </summary>
        public ProgressBarTask CurrentTask
        {
            get
            {
                return (ProgressBarTask)SafelyGetValue(CurrentTaskProperty);
            }

            protected set
            {
                // Set the key, not the property (for read only properties)
                SafelySetValue(CurrentTaskPropertyKey, value);
            }
        }

        /// <summary>
        /// Gets or sets the current subtask. 
        /// </summary>
        public ProgressBarSubtask CurrentSubtask
        {
            get
            {
                return (ProgressBarSubtask)SafelyGetValue(CurrentSubtaskProperty);
            }

            protected set
            {
                // Set the key, not the property (for read only properties)
                SafelySetValue(CurrentSubtaskPropertyKey, value);
            }
        }

        /// <summary>
        /// Gets or sets the list of all tasks.
        /// </summary>
        public ObservableCollection<ProgressBarTask> Tasks
        {
            get
            {
                return (ObservableCollection<ProgressBarTask>)SafelyGetValue(TasksProperty);
            }

            set
            {
                // Remove the event listener from the old collection
                var old = (ObservableCollection<ProgressBarTask>)SafelyGetValue(TasksProperty);
                if (old != null)
                    old.CollectionChanged -= Tasks_CollectionChanged;

                // Set our value
                SafelySetValue(TasksProperty, value);

                // Add the event listener to the new collection
                value.CollectionChanged += Tasks_CollectionChanged;
            }
        }

        /// <summary>
        /// Gets or sets the list of all DynamicTasks. Dynamic tasks are tasks
        /// that do not effect the overall progress of the primary progress
        /// bar in any way. They will, however, display a subtask bar if the
        /// dynamic task has one or more subtasks. DynamicTasks can be added
        /// at any time during the execution of the progress bar, and will
        /// always be executed before any standard tasks or subtasks.
        /// </summary>
        public ObservableCollection<ProgressBarTask> DynamicTasks
        {
            get
            {
                return (ObservableCollection<ProgressBarTask>)SafelyGetValue(DynamicTasksProperty);
            }

            set
            {
                SafelySetValue(DynamicTasksProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the total number of primary tasks we have to complete. 
        /// This value represents the maximum number of steps the primary task 
        /// progress bar has. This is a combination of subtasks and primary tasks.
        /// </summary>
        public double TotalTasks
        {
            get
            {
                return (double)SafelyGetValue(TotalTasksProperty);
            }

            protected set
            {
                // Set the key, not the property (for read only properties)
                SafelySetValue(TotalTasksPropertyKey, value);
            }
        }

        /// <summary>
        /// Gets or sets the total number of tasks that we've completed. This 
        /// value effects how full the primary progrss bar is. This value is 
        /// a combination of subtasks and primary tasks completed.
        /// </summary>
        public double TotalTasksCompleted
        {
            get
            {
                return (double)SafelyGetValue(TotalTasksCompletedProperty);
            }

            protected set
            {
                // Set the key, not the property (for read only properties)
                SafelySetValue(TotalTasksCompletedPropertyKey, value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not to display a counter for 
        /// the the total and completed primary tasks. It will looks something 
        /// like this: (1 / 12)
        /// </summary>
        public bool ShowTaskCounter
        {
            get
            {
                return (bool)SafelyGetValue(ShowTaskCounterProperty);
            }

            set
            {
                SafelySetValue(ShowTaskCounterProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not to display a counter 
        /// for the the total and completed subtasks. It will looks something 
        /// like this: (1 / 12)
        /// </summary>
        public bool ShowSubtaskCounter
        {
            get
            {
                return (bool)SafelyGetValue(ShowSubtaskCounterProperty);
            }

            set
            {
                SafelySetValue(ShowSubtaskCounterProperty, value);
            }
        }

        /// <summary>
        /// <para>
        /// Gets or sets a readonly property for displaying the primary task counter string.
        /// </para>
        /// <para>
        /// Readonly dependency properties still need to have setters, and 
        /// cannot just use "PropertyChanged()", because of how template 
        /// binding works. {TemplateBinding} does not actually call the CLR 
        /// getter method. It bypasses this and goes straight to 
        /// DependencyProperty.SafelyGetValue(). Calling PropertyChanged() will not
        /// update a TemplateBinding's value by looking at the CLR getter.
        /// If we want that functionality, we need to use standard binding {Binding}.
        /// Check out this discussion for more info:
        /// http://dotnetslackers.com/WPF/re-131262_WPF_Read_only_Dependency_Properties.aspx
        /// </para>
        /// </summary>
        public string TaskCounterString
        {
            get
            {
                return (string)SafelyGetValue(TaskCounterStringProperty);
            }

            protected set
            {
                // Set the key, not the property (for read only properties)
                SafelySetValue(TaskCounterStringPropertyKey, value);
            }
        }

        /// <summary>
        /// <para>
        /// Gets or sets a readonly property for displaying the subtask counter string.
        /// </para>
        /// <para>
        /// Take a look at my detailed comments above PrimaryTaskCounterString
        /// for more info regarding readonly dependency properties and their
        /// CLR wrappers.
        /// </para>
        /// </summary>
        public string SubtaskCounterString
        {
            get
            {
                return (string)SafelyGetValue(SubtaskCounterStringProperty);
            }

            protected set
            {
                // Set the key, not the property (for read only properties)
                SafelySetValue(SubtaskCounterStringPropertyKey, value);
            }
        }

        #endregion

        #region Dependency Property Registers

        /// <summary>
        /// <para>
        /// Dependency property for ProgressBarWidth.
        /// </para>
        /// <para>
        /// It's incredibly important width is of type double, and not int.
        /// Please see my notes above, over the ProgressBarWidth property
        /// for more info.
        /// </para>
        /// </summary>
        public static readonly DependencyProperty ProgressBarWidthProperty = DependencyProperty.Register(
            "ProgressBarWidth",
            typeof(double),
            typeof(DualProgressBars),
            new FrameworkPropertyMetadata(DefaultProgressBarWidth));

        /// <summary>
        /// Dependency property for Tasks.
        /// </summary>
        public static readonly DependencyProperty TasksProperty = DependencyProperty.Register(
            "Tasks",
            typeof(ObservableCollection<ProgressBarTask>),
            typeof(DualProgressBars),
            new FrameworkPropertyMetadata(new ObservableCollection<ProgressBarTask>()));

        /// <summary>
        /// Dependency property for DynamicTasks.
        /// </summary>
        public static readonly DependencyProperty DynamicTasksProperty = DependencyProperty.Register(
            "DynamicTasks",
            typeof(ObservableCollection<ProgressBarTask>),
            typeof(DualProgressBars),
            new FrameworkPropertyMetadata(new ObservableCollection<ProgressBarTask>()));

        /// <summary>
        /// Dependency property for ShowTaskCounter.
        /// </summary>
        public static readonly DependencyProperty ShowTaskCounterProperty = DependencyProperty.Register(
            "ShowTaskCounter",
            typeof(bool),
            typeof(DualProgressBars),
            new FrameworkPropertyMetadata(true));

        /// <summary>
        /// Dependency property for ShowSubtaskCounter.
        /// </summary>
        public static readonly DependencyProperty ShowSubtaskCounterProperty = DependencyProperty.Register(
            "ShowSubtaskCounter",
            typeof(bool),
            typeof(DualProgressBars),
            new FrameworkPropertyMetadata(true));

        #endregion

        #region Read-Only Dependency Property Registers

        /// <summary>
        /// <para>
        /// Registered as read only. Make sure your setter is protected
        /// when you create the getter/setter wrapper. Also, make sure
        /// your DependencyProperty definition occurs BELOW the 
        /// DependencyPropertyKey definition. It will throw exceptions
        /// if it's the other way around in the code. This was an interesting
        /// bug to track down. Derp.
        /// http://stackoverflow.com/questions/1122595/how-do-you-create-a-read-only-dependency-property
        /// </para>
        /// <para>
        /// Read-only dependency property key for TaskCounterString.
        /// </para>
        /// </summary>
        public static readonly DependencyPropertyKey TaskCounterStringPropertyKey = DependencyProperty.RegisterReadOnly(
            "TaskCounterString",
            typeof(string),
            typeof(DualProgressBars),
            new FrameworkPropertyMetadata(""));

        /// <summary>
        /// Read-only dependency property key for SubtaskCounterString.
        /// </summary>
        public static readonly DependencyPropertyKey SubtaskCounterStringPropertyKey = DependencyProperty
            .RegisterReadOnly(
                "SubtaskCounterString",
                typeof(string),
                typeof(DualProgressBars),
                new FrameworkPropertyMetadata(""));

        /// <summary>
        /// Read-only dependency property key for CurrentTask.
        /// </summary>
        public static readonly DependencyPropertyKey CurrentTaskPropertyKey = DependencyProperty.RegisterReadOnly(
            "CurrentTask",
            typeof(ProgressBarTask),
            typeof(DualProgressBars),
            new FrameworkPropertyMetadata(null));

        /// <summary>
        /// Read-only dependency property key for CurrentSubtask.
        /// </summary>
        public static readonly DependencyPropertyKey CurrentSubtaskPropertyKey = DependencyProperty.RegisterReadOnly(
            "CurrentSubtask",
            typeof(ProgressBarSubtask),
            typeof(DualProgressBars),
            new FrameworkPropertyMetadata(null));

        /// <summary>
        /// Read-only dependency property key for TotalTasks.
        /// </summary>
        public static readonly DependencyPropertyKey TotalTasksPropertyKey = DependencyProperty.RegisterReadOnly(
            "TotalTasks",
            typeof(double),
            typeof(DualProgressBars),
            new FrameworkPropertyMetadata(0.0));

        /// <summary>
        /// Read-only dependency property key for TotalTasksCompleted.
        /// </summary>
        public static readonly DependencyPropertyKey TotalTasksCompletedPropertyKey = DependencyProperty
            .RegisterReadOnly(
                "TotalTasksCompleted",
                typeof(double),
                typeof(DualProgressBars),
                new FrameworkPropertyMetadata(0.0));

        /// <summary>
        /// <para>
        /// Read-only dependency property for TaskCounterString.
        /// </para>
        /// <para>
        /// This must have a matching DependencyPropertyKey, and be declared BELOW
        /// the key in code. 
        /// </para>
        /// </summary>
        public static readonly DependencyProperty TaskCounterStringProperty =
            TaskCounterStringPropertyKey.DependencyProperty;

        /// <summary>
        /// <para>
        /// Read-only dependency property for SubtaskCounterString.
        /// </para>
        /// <para>
        /// This must have a matching DependencyPropertyKey, and be declared BELOW
        /// the key in code. 
        /// </para>
        /// </summary>
        public static readonly DependencyProperty SubtaskCounterStringProperty =
            SubtaskCounterStringPropertyKey.DependencyProperty;

        /// <summary>
        /// <para>
        /// Read-only dependency property for CurrentTask.
        /// </para>
        /// <para>
        /// This must have a matching DependencyPropertyKey, and be declared BELOW
        /// the key in code. 
        /// </para>
        /// </summary>
        public static readonly DependencyProperty CurrentTaskProperty = CurrentTaskPropertyKey.DependencyProperty;

        /// <summary>
        /// <para>
        /// Read-only dependency property for CurrentSubtask.
        /// </para>
        /// <para>
        /// This must have a matching DependencyPropertyKey, and be declared BELOW
        /// the key in code. 
        /// </para>
        /// </summary>
        public static readonly DependencyProperty CurrentSubtaskProperty = CurrentSubtaskPropertyKey.DependencyProperty;

        /// <summary>
        /// <para>
        /// Read-only dependency property for TotalTasks.
        /// </para>
        /// <para>
        /// This must have a matching DependencyPropertyKey, and be declared BELOW
        /// the key in code. 
        /// </para>
        /// </summary>
        public static readonly DependencyProperty TotalTasksProperty = TotalTasksPropertyKey.DependencyProperty;

        /// <summary>
        /// <para>
        /// Read-only dependency property for TotalTasksCompleted.
        /// </para>
        /// <para>
        /// This must have a matching DependencyPropertyKey, and be declared BELOW
        /// the key in code. 
        /// </para>
        /// </summary>
        public static readonly DependencyProperty TotalTasksCompletedProperty =
            TotalTasksCompletedPropertyKey.DependencyProperty;

        #endregion

        #region Public Methods

        /// <summary>
        /// Moves our DualProgressBars control onto the next task or subtask.
        /// </summary>
        public void NextTask()
        {
            NextTaskInternal(false);
        }

        /// <summary>
        /// Moves our DualProgressBars control onto the next task or subtask.
        /// If the next task has a subtask to display, the subtaskName and 
        /// subtaskDetails provided will be used instead of whatever the subtask
        /// was originally configured to have. This is simply a convenience
        /// method that allows the programmer to update subtask names and details
        /// on the fly, instead of configuring them all at the start of the program.
        /// </summary>
        /// <param name="subtaskName"></param>
        /// <param name="subtaskDetails"></param>
        public void NextTask(string subtaskName, string subtaskDetails)
        {
            NextTaskInternal(true, subtaskName, subtaskDetails);
        }

        /// <summary>
        /// <para>
        /// Sets our progress bars to full, and displays a final message. The task
        /// counters will be set to empty.
        /// </para>
        /// <para>
        /// Make sure this is called on the UIDispatcher thread.
        /// </para>
        /// <para>
        /// This is accomplished by creating a new task, that will only be used 
        /// temporarily. It will not be added to our task list or anything else. 
        /// If you want to resume the original build process after setting a
        /// final message (for whatever strange reason), simply call NextTask()
        /// as usual.
        /// </para>
        /// </summary>
        /// <param name="finalPrimaryTaskMessage"></param>
        public void FinalTask(string finalPrimaryTaskMessage)
        {
            // Create a new, temporary, primary task
            CurrentTask = new ProgressBarTask(finalPrimaryTaskMessage, "", 0);

            // Update our total tasks and tasks completed to set the progress bar 
            // to "full"
            TotalTasksCompleted = 0;
            TotalTasks = 0;

            // Set our counter string to empty
            TaskCounterString = "";
        }

        /// <summary>
        /// Resets all data for this control. 
        /// </summary>
        /// <param name="removeAllTasks">
        /// Whether or not to remove all tasks from the Tasks list. If set to false,
        /// the tasks are not removed from the list, but the IsComplete property
        /// for all tasks and subtasks will be reset to false. Dynamic tasks will
        /// always be removed, regardless of this value.
        /// </param>
        public void Reset(bool removeAllTasks)
        {
            // Remove all dynamic tasks
            DynamicTasks.Clear();

            // Remove all tasks is specified. Otherwise, reset the IsComplete flag
            // for all tasks and subtasks.
            if (removeAllTasks)
            {
                Tasks.Clear();
            }
            else
            {
                // Set all tasks and subtasks to incomplete
                foreach (ProgressBarTask task in Tasks)
                {
                    // Also remember to set the SubtasksCompleted to 0
                    task.IsComplete = false;
                    task.SubtasksCompleted = 0;

                    foreach (ProgressBarSubtask subtask in task.Subtasks)
                    {
                        subtask.IsComplete = false;
                    }
                }
            }

            // Reset the total tasks completed. Also set the TotalTasks to 1. We
            // only do this to make the progress bar appear "empty" instead of
            // "full" after a reset, and before we've called NextTask().
            TotalTasksCompleted = 0;
            TotalTasks = 1;

            // Reset the CurrentTask and CurrentSubtask
            CurrentTask = null;
            CurrentSubtask = null;

            // Update our counter strings
            UpdateCounterStrings();
        }

        /// <summary>
        /// Prints a debug line to the console. Used for debugging total tasks
        /// complete/incomplete count.
        /// </summary>
        public void PrintTotalTaskDebugInfo()
        {
            // Determine which task we are currently on. If we've completed all tasks,
            // just say we're still on the last task to avoid confusion.
            double currentTask = TotalTasksCompleted + 1;
            if (currentTask > TotalTasks)
                currentTask = TotalTasks;

            // Print the debug info to the console
            Console.WriteLine(
                "Total Tasks: " +
                TotalTasks +
                ", Currently on Task: " +
                (int)currentTask);
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// The private implementation for NextTask. Moves our DualProgressBars to
        /// the next task or subtask.
        /// </summary>
        /// <param name="overwriteSubtaskInfo"></param>
        /// <param name="subtaskName"></param>
        /// <param name="subtaskDetails"></param>
        private void NextTaskInternal(bool overwriteSubtaskInfo, string subtaskName = "", string subtaskDetails = "")
        {
            // If we have a current task, either mark it, or one of it's subtasks
            // as complete.
            MarkTaskOrSubtaskAsComplete();

            // Now that we've marked the previous task and/or subtask as complete,
            // get a reference to the next task.
            CurrentTask = GetCurrentTask();

            // Also get a reference to the new subtask, but don't set it to the
            // CurrentSubtask yet. We want to update its on-the-fly values (if 
            // there are any) before we set it to the CurrentSubtask to avoid
            // UI flicker. Occasionally, you will see "Default Subtask" for a 
            // split second if you don't update the on-the-fly values first.
            ProgressBarSubtask oldSubtask = CurrentSubtask;
            ProgressBarSubtask newSubtask = GetCurrentSubtask();

            // Update the on-the-fly values if they were set. Remember that this 
            // is simply a convenience that allows the programmer to update 
            // subtask names and details on the fly, instead of configuring them 
            // all at the start of the program. Only do this however, if the subtask
            // actually changed. If we've reached the end of the list, and the subtask
            // did not change when we called GetCurrentSubtask(), then we shouldn't
            // update the subtasks' values.
            if (overwriteSubtaskInfo && newSubtask != null && oldSubtask != newSubtask)
            {
                newSubtask.TaskName = subtaskName;
                newSubtask.TaskDetails = subtaskDetails;
            }

            // Now that we've set the on-the-fly values, we can update the 
            // CurrentSubtask reference.
            CurrentSubtask = newSubtask;

            // Always recount our total tasks/subtasks, and the number of 
            // tasks/subtasks we have completed. We do not want our values to get
            // out of sync with the collection.
            RecountAll();

            // Sleep for a short duration if the user has configured this control
            // to do so (only available from a worker thread, not the UI thread)
            SleepForReadability();
        }

        /// <summary>
        /// Recounts the total number of tasks and subtasks completed. We want to run
        /// this method during every NextTask() call instead of keeping a counters
        /// and simply performing a ++ operation every time we call NextTask. The 
        /// reason for this is because the user could manually mark a task as complete.
        /// We don't want to be running hidden counters that could get out of sync
        /// with the real number of completed tasks and subtasks.
        /// </summary>
        private void RecountTasksAndSubtasksCompleted()
        {
            double result = 0.0;

            foreach (ProgressBarTask task in Tasks)
            {
                // Recount the subtasks completed as well
                task.SubtasksCompleted = task.Subtasks.Count(x => x.IsComplete);

                // If the task has one or more subtasks, add the number of its 
                // completed subtasks to our total result. Otherwise, just add
                // one for the completed primary task (but only if it's complete).
                if (task.HasSubtask)
                    result += task.SubtasksCompleted;
                else if (task.IsComplete)
                    result++;
            }

            TotalTasksCompleted = result;
        }

        /// <summary>
        /// Attempts do determine which task we should currently be working on.
        /// It will find the first incomplete task or dynamic task, if any exist.
        /// Dynamic tasks will always have priority and be returned before 
        /// standard tasks.
        /// </summary>
        /// <returns>
        /// The <see cref="ProgressBarTask"/>. DynamicTasks will be returned
        /// first if they exist. If no standard tasks or dynamic tasks exist,
        /// this method will return Null.
        /// </returns>
        private ProgressBarTask GetCurrentTask()
        {
            // If any dynamic tasks exist, always complete them first
            foreach (ProgressBarTask task in DynamicTasks)
            {
                if (!task.IsComplete)
                    return task;
            }

            // Next, search for the first standard task that has not been completed.
            foreach (ProgressBarTask task in Tasks)
            {
                if (!task.IsComplete)
                    return task;
            }

            // If all tasks are complete, return the last task in the list. Don't
            // return null. We want to keep the last task visible on the UI until
            // the progress bars are hidden.
            return Tasks.Last();
        }

        /// <summary>
        /// Attempts do determine which subtask we should currently be working on.
        /// It will find the first incomplete subtask of the CurrentTask.
        /// </summary>
        /// <returns>
        /// The <see cref="ProgressBarSubtask"/>. If the CurrentTask is null, or the
        /// CurrentTask does not contain any subtasks, this method will return Null.
        /// </returns>
        private ProgressBarSubtask GetCurrentSubtask()
        {
            // The CurrentTask should have been calculated before this method is
            // called. If it is null, that means we have no tasks, and thus no
            // subtasks. Also return null if no subtasks exist.
            if (CurrentTask == null || CurrentTask.Subtasks.Count == 0)
                return null;

            // Search for and return the first subtask that has not been completed.
            // Return null if there are no subtasks currently available to work on.
            foreach (ProgressBarSubtask subtask in CurrentTask.Subtasks)
            {
                if (!subtask.IsComplete)
                    return subtask;
            }

            // If all subtasks are complete, return the last subtask in the list. 
            // Don't return null. We want to keep the last task visible on the UI 
            // until the progress bars are hidden.
            return CurrentTask.Subtasks.Last();
        }

        /// <summary>
        /// <para>
        /// Before moving on to the next task or subtask, we want to mark the 
        /// currently selected task or subtask as completed. 
        /// </para>
        /// <para>
        /// If the current task has one or more incomplete subtasks, it will
        /// mark the first one as complete. If the current does not have any
        /// subtasks, or all of its subtasks have been completed, then it will
        /// be marked as completed as well.
        /// </para>
        /// </summary>
        private void MarkTaskOrSubtaskAsComplete()
        {
            if (CurrentTask == null)
                return;

            // Check to see if the task has any unfinished subtasks. If it does,
            // mark a subtask as complete.
            foreach (ProgressBarSubtask subtask in CurrentTask.Subtasks)
            {
                if (!subtask.IsComplete)
                {
                    subtask.IsComplete = true;
                    CurrentTask.SubtasksCompleted++;
                    break;
                }
            }

            // Check to see if the task has any subtasks left to complete
            bool hasIncompleteSubtask = CurrentTask.Subtasks.Any(subtask => !subtask.IsComplete);

            // If the task has no more subtasks to complete, mark it as completed
            // as well.
            if (!hasIncompleteSubtask)
                CurrentTask.IsComplete = true;
        }

        /// <summary>
        /// Recounts all of our tasks and updates the UI strings.
        /// </summary>
        private void RecountAll()
        {
            // Recount all of our completed and incomplete tasks
            CalculateTotalTasks();
            RecountTasksAndSubtasksCompleted();

            // Update our counter strings for the UI
            UpdateCounterStrings();
        }

        /// <summary>
        /// Updates the primary and subtask counter strings. This method should
        /// be called every time we move to the next task.
        /// </summary>
        private void UpdateCounterStrings()
        {
            // If the current task is null, don't display any counter strings
            if (CurrentTask == null)
            {
                TaskCounterString = "";
                SubtaskCounterString = "";
                return;
            }

            // Count the number of primary tasks that have been completed.
            // If we haven't completed all tasks yet, add 1, because we
            // always want to start at 1, and not 0. Don't add 1 if all
            // tasks have been completed however, because then it will
            // look like "(5 / 4)" (wrong).
            int tasksComplete = Tasks.Count(x => x.IsComplete);
            if (tasksComplete != Tasks.Count)
                tasksComplete++;

            // Update the primary task counter string
            TaskCounterString = "( " + tasksComplete + " / " + Tasks.Count + " )";

            // If the CurrentTask is null, don't display any subtask info
            if (CurrentTask == null)
                return;

            // Count the number of subtasks that have been completed.
            // If we haven't completed all subtasks yet, add 1, because we
            // always want to start at 1, and not 0. Don't add 1 if all
            // subtasks have been completed however, because then it will
            // look like "(5 / 4)" (wrong).
            int subtasksComplete = CurrentTask.Subtasks.Count(x => x.IsComplete);
            if (subtasksComplete != CurrentTask.Subtasks.Count)
                subtasksComplete++;

            // Update the subtask counter string
            SubtaskCounterString = "( " + subtasksComplete + " / " + CurrentTask.Subtasks.Count + " )";
        }

        /// <summary>
        /// Counts the total number of tasks we have to complete. This is a 
        /// combination of all primary tasks and subtasks. However, note that
        /// if a primary task has several subtasks, we only count the subtasks.
        /// The primary task itself is not included in the count if it has 
        /// subtasks.
        /// </summary>
        private void CalculateTotalTasks()
        {
            // Reset our total tasks to 0
            TotalTasks = 0;

            // Don't add anything if our tasks are null
            if (Tasks == null)
                return;

            // Iterate over each task. If the task doesn't have any subtasks, 
            // just add 1. Otherwise, add the total number of subtasks. Don't 
            // count the primary task itself if it has subtasks.
            foreach (ProgressBarTask task in Tasks)
            {
                if (task.Subtasks.Count == 0)
                    TotalTasks += 1;
                else
                    TotalTasks += task.Subtasks.Count;
            }
        }

        #endregion

        #region Event Listeners

        /// <summary>
        /// Any time the Tasks collection changes, we should do a recount on all 
        /// complete/incomplete tasks.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Tasks_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            RecountAll();
        }

        #endregion
    }
}