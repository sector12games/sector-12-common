﻿// -----------------------------------------------------------------------
// <copyright file="WorkerThreadSafeContentControl.cs" company="Sector12">
//     Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Sector12Common.Controls
{
    using System;
    using System.Threading;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Threading;

    /// <summary>
    /// Class definition for WorkerThreadSafeContentControl.cs
    /// </summary>
    public class WorkerThreadSafeContentControl : ContentControl
    {
        private static int _readabilityDelay;
        private static Dispatcher _dispatcher;

        /// <summary>
        /// <para>
        /// Progress bars will typically be used when we want to execute a long
        /// task in a worker thread. We will typically want to update the progress
        /// bars from within the worker thread as tasks are completed. Because
        /// this is a UIElement, we need to make sure to make all updates to it
        /// occur in the UI Dispatcher thread. 
        /// </para>
        /// <para>
        /// This is a convenience method that allows you to call updates without
        /// having to worry about which tghread you are on. Simply call this
        /// initialize method before using your progress bars, and you can safely
        /// update it's values from within a worker thread.
        /// </para>
        /// <para>
        /// You can also provide this method with an optional readabilityDelay.
        /// This optional parameter will put the current thread to sleep for a
        /// short duration after each of the following method calls:
        ///   - NextPrimaryTask() 
        ///   - NextSubtask()
        ///   - NextPrimaryTaskUpdateDetailsOnly()
        ///   - NextSubtaskUpdateDetailsOnly()
        /// This delay is used in order for the user to be able to read what 
        /// the progress bars are actually displaying before jumping to the next 
        /// task. This value is only used if this control has been initialized 
        /// for use in a worker thread (by calling this method).
        /// </para>
        /// </summary>
        /// <param name="dispatcher"></param>
        /// <param name="readabilityDelay"></param>
        public void InitializeForUseInWorkerThread(Dispatcher dispatcher, int readabilityDelay = 0)
        {
            _dispatcher = dispatcher;
            _readabilityDelay = readabilityDelay;
        }

        /// <summary>
        /// Attempts to set dependency property values. This is done on the
        /// Dispatcher thread if the dispatcher has been set. Otherwise, it
        /// is done on the currently executing thread.
        /// </summary>
        /// <param name="property"></param>
        /// <param name="value"></param>
        protected void SafelySetValue(DependencyProperty property, object value)
        {
            if (_dispatcher != null)
                UISet(() => SetValue(property, value));
            else
                SetValue(property, value);
        }

        /// <summary>
        /// Attempts to set the value of the dependency property key. This is for
        /// read-only dependency properties. This is done on the Dispatcher thread 
        /// if the dispatcher has been set. Otherwise, it is done on the currently 
        /// executing thread.
        /// </summary>
        /// <param name="propertyKey"></param>
        /// <param name="value"></param>
        protected void SafelySetValue(DependencyPropertyKey propertyKey, object value)
        {
            if (_dispatcher != null)
                UISet(() => SetValue(propertyKey, value));
            else
                SetValue(propertyKey, value);
        }

        /// <summary>
        /// Attempts to get the value of the depency property. This is done on the 
        /// Dispatcher thread if the dispatcher has been set. Otherwise, it is 
        /// done on the currently executing thread.
        /// </summary>
        /// <param name="property"></param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        protected object SafelyGetValue(DependencyProperty property)
        {
            if (_dispatcher != null)
                return UIGet(() => GetValue(property));
            else
                return GetValue(property);
        }

        /// <summary>
        /// Executes the provided delegate on the UIDispatcher thread. The code
        /// used to call this function is pretty damn sexy and compact. Check out
        /// these discussions if you want to read more about how to call delegates
        /// using lamda expressions and anonymous functions.
        /// http://stackoverflow.com/questions/4621623/wpf-multithreading-ui-dispatcher-in-mvvm
        /// http://stackoverflow.com/questions/2082615/pass-method-as-parameter-using-c-sharp
        /// </summary>
        /// <param name="func"></param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        protected object UIGet(Func<object> func)
        {
            // Be careful. You can pass in any delegate into dispatcher.Invoke().
            // This means you can pass in either an Action or a Func<>. If you 
            // pass in an Action, you won't get a return value back. The return
            // value will be ignored because Action doesn't support return values.
            // That's why the parameter for this method is a Func<object> and
            // not an action or standard delegate. I want to ensure we can get
            // a return value back from the invoke.
            return _dispatcher.Invoke(DispatcherPriority.Normal, func);
        }

        /// <summary>
        /// Executes the provided delegate on the UIDispatcher thread. The code
        /// used to call this function is pretty damn sexy and compact. Check out
        /// these discussions if you want to read more about how to call delegates
        /// using lamda expressions and anonymous functions.
        /// http://stackoverflow.com/questions/4621623/wpf-multithreading-ui-dispatcher-in-mvvm
        /// http://stackoverflow.com/questions/2082615/pass-method-as-parameter-using-c-sharp
        /// </summary>
        /// <param name="action"></param>
        protected void UISet(Action action)
        {
            // We are calling a setter, and thus we don't expect to get a 
            // return value back. This is why we can use Action instead of
            // Func<>. Action should be used when we don't want or need a 
            // return value. Action is the same as Action<>, but with no
            // input parameters.
            _dispatcher.Invoke(DispatcherPriority.Normal, action);
        }

        /// <summary>
        /// <para>
        /// Sleeps for a short period of time so that the user can see what
        /// message our progress bars are displaying if tasks are being completed
        /// too fast. 
        /// </para>
        /// <para>
        /// This will only occur if InitializeForUseInWorkerThread() has been
        /// called, and the thread calling this function is not the UIDispatcher
        /// thread. Essentially, we don't ever want to sleep on the UI thread,
        /// only on the worker thread.
        /// </para>
        /// </summary>
        protected void SleepForReadability()
        {
            if (_dispatcher != null && _dispatcher.Thread != Thread.CurrentThread)
                Thread.Sleep(_readabilityDelay);
        }
    }
}
