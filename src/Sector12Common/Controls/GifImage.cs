﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GifImage.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Sector12Common.Controls
{
    using System;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media.Animation;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// <para>
    /// A control for displaying an animated gif.
    /// </para>
    /// <para>
    /// http://stackoverflow.com/questions/210922/how-do-i-get-an-animated-gif-to-work-in-wpf
    /// http://stackoverflow.com/questions/4575247/reading-metadata-property-of-gifbitmapdecoder-why-is-it-null
    /// </para>
    /// </summary>
    public class GifImage : Image
    {
        public static readonly DependencyProperty IsPausedProperty = 
            DependencyProperty.Register(
                "IsPaused", 
                typeof(bool), 
                typeof(GifImage), 
                new UIPropertyMetadata(false, ChangingPauseValue));

        public static readonly DependencyProperty AllowClickToPauseProperty = 
            DependencyProperty.Register(
                "AllowClickToPause", 
                typeof(bool), 
                typeof(GifImage), 
                new UIPropertyMetadata(true));

        public static readonly DependencyProperty OmitFirstFrameWhileAnimatingProperty =
            DependencyProperty.Register(
                "OmitFirstFrameWhileAnimating", 
                typeof(bool), 
                typeof(GifImage), 
                new UIPropertyMetadata(false, ChangingAnimationValues));

        public static readonly DependencyProperty FrameIndexProperty =
            DependencyProperty.Register(
                "FrameIndex", 
                typeof(int), 
                typeof(GifImage), 
                new UIPropertyMetadata(0, ChangingFrameIndex));

        public static readonly DependencyProperty AnimationSpeedMultiplierProperty =
            DependencyProperty.Register(
                "AnimationSpeedMultiplier", 
                typeof(double), 
                typeof(GifImage), 
                new UIPropertyMetadata(1.0, ChangingAnimationValues));

        public static readonly DependencyProperty GifUriProperty =
            DependencyProperty.Register(
                "GifUri", 
                typeof(Uri), 
                typeof(GifImage), 
                new UIPropertyMetadata(null, ChangingAnimationValues));

        /// <summary>
        /// Individual animations don't have basic abilities, like the ability to
        /// Pause or Resume themselves. This is where storyboards come into play.
        /// A storyboard monitors groups of animations (timelines) and can tell
        /// them how to behave (begin, stop, pause, resume, etc).
        /// </summary>
        private Storyboard _storyboard;

        private GifBitmapDecoder _decoder;
        private Int32Animation _animation;

        /// <summary>
        /// Initializes a new instance of the <see cref="GifImage"/> class.
        /// </summary>
        public GifImage()
        {
            MouseDown += GifImage_MouseDown;
            _storyboard = new Storyboard();
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not the gif animation is 
        /// currently paused.
        /// </summary>
        public bool IsPaused
        {
            get
            {
                return (bool)GetValue(IsPausedProperty);
            }

            set
            {
                SetValue(IsPausedProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not the gif animation 
        /// can be paused/unpaused when clicked.
        /// </summary>
        public bool AllowClickToPause
        {
            get
            {
                return (bool)GetValue(AllowClickToPauseProperty);
            }

            set
            {
                SetValue(AllowClickToPauseProperty, value);
            }
        }

        /// <summary>
        /// <para>
        /// Gets or sets a value indicating whether or not to omit the first frame
        /// when animating.
        /// </para>
        /// <para>
        /// Some gifs might have an initial starting frame that should be displayed
        /// before the animation starts. After the animation begins (is unpaused)
        /// we should not see that initial frame again. It's not really a part of
        /// the animation.
        /// </para>
        /// <para>
        /// For example, I use this with my "dave humping" gif. The first frame is a
        /// picture of him standing still. After the animation starts, it's just
        /// him humping. He never stands still again, unless I completely reset
        /// the animation (change the GifUri or call ResetAnimation()).
        /// </para>
        /// </summary>
        public bool OmitFirstFrameWhileAnimating
        {
            get
            {
                return (bool)GetValue(OmitFirstFrameWhileAnimatingProperty);
            }

            set
            {
                SetValue(OmitFirstFrameWhileAnimatingProperty, value);
            }
        }

        /// <summary>
        /// <para>
        /// Gets or sets the current frame of the animation. This value will be 
        /// animated (changed) by the WPF animation system.
        /// </para>
        /// <para>
        /// In general, this shouldn't be set manually. However, for longer animations, 
        /// you can set this value to start in the middle or at an arbitrary frame 
        /// if desired. 
        /// </para>
        /// </summary>
        public int FrameIndex
        {
            get
            {
                return (int)GetValue(FrameIndexProperty);
            }

            set
            {
                SetValue(FrameIndexProperty, value);
            }
        }

        /// <summary>
        /// <para>
        /// Gets or sets the animation's speed multiplier.
        /// </para>
        /// <para>
        /// The gif can be animated faster or slower than the image was originally
        /// configured to run. A number greater than 1 will speed it up, a number
        /// less than 1 will slow it down.
        /// </para>
        /// </summary>
        public double AnimationSpeedMultiplier
        {
            get
            {
                return (double)GetValue(AnimationSpeedMultiplierProperty);
            }

            set
            {
                SetValue(AnimationSpeedMultiplierProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the URI of the gif we want to animate.
        /// </summary>
        public Uri GifUri
        {
            get
            {
                return (Uri)GetValue(GifUriProperty);
            }

            set
            {
                SetValue(GifUriProperty, value);
            }
        }

        /// <summary>
        /// Gets the animation's gif decoder. We can use this object to determine 
        /// information about our gif, such as how many frames it contains, etc.
        /// </summary>
        public GifBitmapDecoder Decoder
        {
            get
            {
                return _decoder;
            }

            private set
            {
                _decoder = value;
            }
        }

        /// <summary>
        /// Gets the animation object for this gif image. Actually, it would be more 
        /// precise to say that this is the animation object for our FrameIndex
        /// property. This WPF animation will change the value of FrameIndex over
        /// time. Every time the FrameIndex changes, our control will draw the
        /// current frame from the gif image. 
        /// </summary>
        public Int32Animation Animation
        {
            get
            {
                return _animation;
            }

            private set
            {
                _animation = value;
            }
        }

        /// <summary>
        /// <para>
        /// Completely resets the current animation. This allows us to start the
        /// animation over at the beginning, and display an omitted first frame
        /// if the OmitFirstFrameWhileAnimating flag is set. 
        /// </para>
        /// <para>
        /// This method does the exact same thing as changing the GifUri.
        /// </para>
        /// </summary>
        public void ResetAnimation()
        {
            BeginNewAndReplaceOldAnimation();
        }

        /// <summary>
        /// Toggles the gif animation's play/pause state.
        /// </summary>
        public void TogglePlayPause()
        {
            IsPaused = !IsPaused;
        }

        /// <summary>
        /// Starts the gif animation.
        /// </summary>
        public void Play()
        {
            IsPaused = false;
        }

        /// <summary>
        /// Pauses the gif animation.
        /// </summary>
        public void Pause()
        {
            IsPaused = true;
        }

        /// <summary>
        /// This is a callback method of GifImage. Every time the FrameIndex property
        /// changes, this will be called.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="ev"></param>
        private static void ChangingFrameIndex(
            DependencyObject obj,
            DependencyPropertyChangedEventArgs ev)
        {
            GifImage control = obj as GifImage;
            control.UpdateVisual(control.FrameIndex);
        }

        /// <summary>
        /// This is a callback method of GifImage. Every time the IsPaused value 
        /// changes, this method should be called. It's important that this is
        /// a callback, and not simply done in the IsPaused setter. The setter
        /// does not always get called by WPF, sometimes WPF sets the property
        /// directly.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="ev"></param>
        private static void ChangingPauseValue(
            DependencyObject obj,
            DependencyPropertyChangedEventArgs ev)
        {
            GifImage control = obj as GifImage;
            control.UpdatePauseValue();
        }

        /// <summary>
        /// This is a callback method of GifImage. Every time the gif uri changes or
        /// our speed multiplier changes, this method will be called. It initializes 
        /// our gif bitmap decoder and animation objects.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="ev"></param>
        private static void ChangingAnimationValues(
            DependencyObject obj,
            DependencyPropertyChangedEventArgs ev)
        {
            // This method is a callback method for GifImage
            GifImage control = obj as GifImage;

            // Start the new animation. We also need to make sure to remove
            // the old animation, otherwise, both will be running at the same time
            // and interfere with each other.
            control.BeginNewAndReplaceOldAnimation();
        }

        /// <summary>
        /// <para>
        /// Calculates the total time it will take to go through one full gif 
        /// rotation (show every frame once). This value is effected by our 
        /// AnimationSpeedMultiplier value. 
        /// </para>
        /// <para>
        /// In most gifs, every frame has the same delay. However, it is possible
        /// for some or all of the frames in a gif to have different delays. Because 
        /// this method calculates every frame of the gif separately, gifs containing 
        /// frames with different delays will still work in this control.
        /// </para>
        /// </summary>
        /// <returns>
        /// The total time to complete one full gif rotation.
        /// </returns>
        private TimeSpan CalculateTotalAnimationDuration()
        {
            double totalTime = 0.0;
            
            int startIndex;
            if (OmitFirstFrameWhileAnimating)
                startIndex = 1;
            else
                startIndex = 0;

            for (int i = startIndex; i < Decoder.Frames.Count; i++)
            {
                // Get the delay for the individual frame
                totalTime += GetIndividualFrameDuration(i);
            }

            // Return a timespan representing the total time it takes for this gif
            // image to go through one full rotation.
            return TimeSpan.FromMilliseconds(totalTime);
        }

        /// <summary>
        /// Gets the duration (delay) of a single frame in our gif. This value 
        /// includes our AnimationSpeedMultiplier. The value returned is in 
        /// milliseconds.
        /// </summary>
        /// <param name="index"></param>
        /// <returns>
        /// Gets the duration of a single frame in our gif, including the speed
        /// multiplier.
        /// </returns>
        private double GetIndividualFrameDuration(int index)
        {
            // Get the individual frame's metadata
            BitmapFrame frame = Decoder.Frames[index];
            BitmapMetadata metaData = (BitmapMetadata)frame.Metadata;

            // Determine the frame's delay (duration). This is in hundreds
            // of a second, so multiply by 10 to get milliseconds.
            ushort durationInHundrethsOfASecond = (ushort)metaData.GetQuery("/grctlext/Delay");
            double msDuration = durationInHundrethsOfASecond * 10;

            // Multiply the frame's built-in duration by our speed modifier.
            // Since we're calculating the frame's delay time, we actually
            // want to divide by the multiplier instead of multiplying. If 
            // our speed is faster, the total duration should get smaller,
            // not larger.
            double finalDuration = msDuration / AnimationSpeedMultiplier;

            return finalDuration;
        }

        /// <summary>
        /// This method will be called every time our WPF Animation ticks. Remember, 
        /// our WPF animation is animating (changing) the FrameIndex, not the gif
        /// itself. Every time our index changes, this method tells our control to
        /// draw a different frame from the gif image.
        /// </summary>
        /// <param name="index"></param>
        private void UpdateVisual(int index)
        {
            if (Decoder != null)
            {
                // I currently have a really nasty bug where my animation 
                // is still occasionally firing when I change my animation
                // variables. This is somehow happening even when paused. I
                // think it has something to do with me beginning a new animation
                // before the old animation is fully cleaned up/disconnected
                // from our control. It is incredibly hard to track down and
                // does not happen all of the time. Speeding up the animation,
                // switching to larrity dancing, pausing it, and then switching
                // to dave will occasionally trigger this issue. This code doesn't
                // fix or explain the bug (it still exists to my great frustration),
                // but it does prevent the control from blowing up. When the issue
                // does arise, it is not noticable by the user.
                if (index > Decoder.Frames.Count)
                    return;

                // Update the image this control will display and tell 
                // it that it needs to redraw itself
                Source = Decoder.Frames[index];
                InvalidateVisual();
            }
        }

        /// <summary>
        /// Pauses or resumes our storyboard. This will be called from a callback
        /// method any time our IsPaused dependency property changes.
        /// </summary>
        private void UpdatePauseValue()
        {
            // Pause or resume the current animation
            if (IsPaused)
                _storyboard.Pause(this);
            else
                _storyboard.Resume(this);
        }

        /// <summary>
        /// Begins a new animation. The old animation will be replaced. Make sure
        /// to replace the old animation, otherwise, both will be running at the 
        /// same time and interfere with each other. 
        /// </summary>
        private void BeginNewAndReplaceOldAnimation()
        {
            // First, clear all old animations out of our storyboard
            _storyboard.Stop(this);
            _storyboard.Children.Clear();

            // Every time our gif uri changes, we need to update our gif decoder 
            // to point to the new image.
            Decoder = new GifBitmapDecoder(
                GifUri,
                BitmapCreateOptions.PreservePixelFormat,
                BitmapCacheOption.Default);

            // Now, determine the total duration of our gif animation. This is the 
            // time it takes to get through one full gif rotation. The decoder we
            // just set will be used during this process.
            TimeSpan duration = CalculateTotalAnimationDuration();

            // Next, create the new WPF animation object. If the 
            // OmitFirstFrameWhileAnimating flag is set, start at index 1 instead
            // of index 0.
            if (OmitFirstFrameWhileAnimating)
                Animation = new Int32Animation(1, Decoder.Frames.Count - 1, new Duration(duration));
            else
                Animation = new Int32Animation(0, Decoder.Frames.Count - 1, new Duration(duration));

            // If we are going to omit the first frame of the animation, don't
            // begin the animation until we've displayed the first frame. The
            // first frame won't be shown again, but we always want to show it
            // when the animation is first started. Because our animation will
            // start at an index of 1, we need to add this short delay.
            if (OmitFirstFrameWhileAnimating)
                Animation.BeginTime = TimeSpan.FromMilliseconds(GetIndividualFrameDuration(0));

            // Make the animation loop forever.
            Animation.RepeatBehavior = RepeatBehavior.Forever;

            // Now, add in the new animation to the storyboard and set the animation's
            // target property (what it will be animating). In our case, WPF will 
            // animate (change) the FrameIndex every time the animation takes a step. 
            // So in other words, we're animating the Index, not the gif itself. 
            // Every time the index changes, we tell our control to draw a different 
            // frame from the gif image.
            _storyboard.Children.Add(Animation);
            Storyboard.SetTarget(Animation, this);
            Storyboard.SetTargetProperty(Animation, new PropertyPath(FrameIndexProperty));

            // Reset our frame index to zero. We want the new animation to
            // start at the beginning. Keep in mind, this won't actually take effect
            // until after this method has finished. The dependency properties getting
            // set happens asynchronously I guess.
            FrameIndex = 0;

            // Update our visual. It's important we pass in 0 here, because our 
            // FrameIndex won't actually be updated to 0 yet. That will happen
            // asynchronously, usually after this method is completed.
            UpdateVisual(0);
            
            // Start the animation. Make sure to specify that the storyboard is
            // controllable. Otherwise, pause/resume/stop operations will be ignored.
            _storyboard.Begin(this, true);

            // We just started the new animation, so we need to make sure to pause
            // it if our GifImage control is currently paused. 
            if (IsPaused)
                _storyboard.Pause(this);
        }

        /// <summary>
        /// Called when this control is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GifImage_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (AllowClickToPause)
                IsPaused = !IsPaused;
        }        
    }
}
