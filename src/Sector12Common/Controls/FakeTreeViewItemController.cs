﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FakeTreeViewItemController.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Sector12Common.Controls
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;

    /// <summary>
    /// Class definition for FakeTreeViewItemController.cs.
    /// </summary>
    public class FakeTreeViewItemController
    {
        private ObservableCollection<FakeTreeViewItem> _currentListViewItems;
        private ListView _listView;

        /// <summary>
        /// Initializes a new instance of the <see cref="FakeTreeViewItemController"/> class.
        /// </summary>
        /// <param name="list"></param>
        /// <param name="listView"></param>
        public FakeTreeViewItemController(IEnumerable<FakeTreeViewItem> list, ListView listView)
        {
            // we really only keep track of the listview for testing purposes
            _listView = listView;

            // put all of the items into a list that will
            // used to fake a treeview
            _currentListViewItems = new ObservableCollection<FakeTreeViewItem>();
            foreach (FakeTreeViewItem item in list)
            {
                item.PropertyChanged += ListItem_PropertyChanged;
                _currentListViewItems.Add(item);

                // if the item is expanded already, add it's children too
                if (item.IsExpanded)
                    AddChildren(item);
            }
        }

        /// <summary>
        /// Gets the flat list of all nodes in this tree.
        /// </summary>
        public IEnumerable<FakeTreeViewItem> AllListViewItems
        {
            get
            {
                return _currentListViewItems;
            }
        }

        /// <summary>
        /// <para>
        /// Gets the total number of data items that are currently loaded in 
        /// the ListView.
        /// </para>
        /// <para>
        /// This will probably be used for testing purposes only.
        /// </para>
        /// <para>
        /// This will return the total number loaded regardless of whether or 
        /// not the items are visible.
        /// </para>
        /// </summary>
        public int DataItemsCount
        {
            get
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets the total number of visual elements for our current 
        /// listview (left or right).
        /// </summary>
        public int VisualItemsCount
        {
            get
            {
                return GetVisualCount(_listView);
            }
        }

        /// <summary>
        /// Again, this method will probably only be used for testing.
        /// It returns the total number of visuals in use for a 
        /// given dependency object. This will be useful for checking
        /// the performance of your ListView (testing virtualization, etc).
        /// </summary>
        /// <param name="visual"></param>
        /// <returns>
        /// The total number of visuals in use.
        /// </returns>
        private static int GetVisualCount(DependencyObject visual)
        {
            int visualCount = 1;
            int childCount = VisualTreeHelper.GetChildrenCount(visual);

            for (int i = 0; i < childCount; i++)
            {
                DependencyObject childVisual = VisualTreeHelper.GetChild(visual, i);
                visualCount += GetVisualCount(childVisual);
            }

            return visualCount;
        }

        /// <summary>
        /// Every item that we insert into the ListView will subscribe to this
        /// event.  By listening for certain property changes on all of our
        /// data items (changes like whether or not the item is expanded)
        /// we can insert or remove items as necessary.  Again this is
        /// all part of trying to fake a treeview.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListItem_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            FakeTreeViewItem keyHolder = (FakeTreeViewItem)sender;

            // add or remove children any time an item changes
            // its IsExpanded property
            if (e.PropertyName == "IsExpanded")
            {
                if (keyHolder.IsExpanded)
                    AddChildren(keyHolder);
                else
                    RemoveChildren(keyHolder);
            }
        }

        /// <summary>
        /// Adds the children of a given data item to our listview.
        /// This is mimicking an "expand" within a treeview.
        /// </summary>
        /// <param name="keyHolder"></param>
        /// <returns>
        /// The total number of children added.
        /// </returns>
        private int AddChildren(FakeTreeViewItem keyHolder)
        {
            int parentIndex = _currentListViewItems.IndexOf(keyHolder);
            int count = 0;
            foreach (FakeTreeViewItem item in keyHolder.Children)
            {
                // add an event handler to each child item as well
                item.PropertyChanged += ListItem_PropertyChanged;

                // insert the item into the list at the appropriate index
                _currentListViewItems.Insert(parentIndex + count + 1, item);

                // if that item had any children that were previously expanded,
                // expand the children as well
                if (item.IsExpanded)
                    count += AddChildren(item);

                // increment our counter
                count++;
            }

            return count;
        }

        /// <summary>
        /// Removes the children of a given data item from our listview.
        /// This is mimicking a "collapse" within a treeview.
        /// </summary>
        /// <param name="keyHolder"></param>
        private void RemoveChildren(FakeTreeViewItem keyHolder)
        {
            // the property changed its state to collapsed, so we need
            // to remove the items from the listview
            foreach (FakeTreeViewItem item in keyHolder.Children)
            {
                // remove the child's children as well
                RemoveChildren(item);

                // remove the child
                _currentListViewItems.Remove(item);
                item.PropertyChanged -= ListItem_PropertyChanged;
            }
        }
    }
}
