﻿// -----------------------------------------------------------------------
// <copyright file="DictionaryExtensions.cs" company="Sector12">
//     Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Sector12Common.ExtensionMethods
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Class definition for DictionaryExtensions.cs
    /// </summary>
    public static class DictionaryExtensions
    {
        /// <summary>
        /// <para>
        /// Adds the elements of the specified dictionary to the existing dictionary. 
        /// Duplicate keys are not allowed, and an exception is thrown if attempted.
        /// </para>
        /// <para>
        /// I created this extension method by slightly modifying the solution found
        /// here:
        /// http://stackoverflow.com/questions/3982448/adding-a-dictionary-to-another
        /// </para>
        /// </summary>
        /// <typeparam name="TK">The generic key type.</typeparam>
        /// <typeparam name="TV">The geneic value type.</typeparam>
        /// <param name="source"></param>
        /// <param name="collection"></param>
        public static void AddRange<TK, TV>(this Dictionary<TK, TV> source, Dictionary<TK, TV> collection)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("collection", "Collection is null.");
            }

            foreach (var item in collection)
            {
                if (!source.ContainsKey(item.Key))
                {
                    source.Add(item.Key, item.Value);
                }
                else
                {
                    throw new ArgumentException("An element with the same key already exists in the dictionary.");
                }
            }
        }
    }
}
