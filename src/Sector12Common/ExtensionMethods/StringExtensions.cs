﻿// -----------------------------------------------------------------------
// <copyright file="StringExtensions.cs" company="Sector12">
//     Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Sector12Common.ExtensionMethods
{
    using System.Globalization;

    /// <summary>
    /// Class definition for StringExtensions.cs
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// Sadly, string.Contains does not have an overload that can handle
        /// IgnoreCase. The best alternative is using the method below. It
        /// is better than converting both strings to upper or lower. There
        /// is at least one more acceptable method for doing this as well,
        /// if you're interested. 
        /// http://stackoverflow.com/questions/444798/case-insensitive-containsstring
        /// http://msdn.microsoft.com/en-us/library/wadk4d6s(v=vs.110).aspx
        /// </summary>
        /// <param name="source"></param>
        /// <param name="value"></param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool ContainsIgnoreCase(this string source, string value)
        {
            if (CultureInfo.CurrentCulture.CompareInfo.IndexOf(source, value, CompareOptions.IgnoreCase) >= 0)
                return true;
            else
                return false;
        } 
    }
}
