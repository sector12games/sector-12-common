﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WpfResourceLoader.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Sector12Common.ResourceLoaders
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Markup;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Resources;
    using System.Xml.Linq;

    /// <summary>
    /// Class definition for WpfResourceLoader.cs.
    /// </summary>
    public static class WpfResourceLoader
    {
        /// <summary>
        /// <para>
        /// Attempts to load a resource from the currently running application. 
        /// In order for this method to work, the resource you are trying to 
        /// load must be marked as "Build Action = Resource". 
        /// </para>
        /// <para>
        /// In addition, the contentType you pass in must match the content
        /// type of the item. Currently supported content types are:
        ///  - Text
        ///  - Xml
        ///  - Xaml
        ///  - Image (System.Drawing.Image)
        ///  - ImageControl (System.Windows.Controls.Image)
        ///  - MemoryStream
        /// </para>
        /// <para>
        /// Originally, this method did not pass in the content type, and used
        /// StreamResourceInfo.contentType after the resource was located.
        /// The drawback of this method however, is that the file extension of
        /// the type passed in had to exist in the windows file associations
        /// table. If it did not, the content type returned would be an empty
        /// string. So we had two options: add a new key to the registry for
        /// every unknown file type passed in, or simply pass in the desired
        /// type as a parameter. I chose the latter.
        /// </para>
        /// <para>
        /// In general, in WPF you'll want to store resources in your project
        /// in one of two ways.
        /// - Build Action = Resource (This build action will embed the file 
        ///                  in the project assembly).
        /// - Build Action = Content (This build action will include the file 
        ///                  in the application package without embedding it 
        ///                  in the project assembly).
        /// </para>
        /// <para>
        /// See this page about WPF Resource Files for more information:
        /// http://msdn.microsoft.com/en-us/library/cc296240%28v=vs.95%29.aspx
        /// </para>
        /// </summary>
        /// <typeparam name="T">The type of the resource to load.</typeparam>
        /// <param name="uri"></param>
        /// <param name="contentType"></param>
        /// <returns>
        /// Returns the loaded resource.
        /// </returns>
        public static T LoadResource<T>(Uri uri, ResourceLoaderContentType contentType)
        {
            T c = default(T);
            StreamResourceInfo sri;

            // First, try to locate the resource
            try
            {
                 sri = Application.GetResourceStream(uri);
            }
            catch (Exception)
            {
                throw new Exception(
                    "The resource at the specified URI was not found. " +
                    "Make sure the resource's Build Action is set to \"Resource\" " +
                    "in the item's property window.");
            }

            // This method loads resources, not other content types. In order to
            // load the resource using this method, it's build action needs to be
            // set to "Resource".
            if (sri == null)
            {
                throw new Exception(
                    "The resource at the specified URI was found, but it's StreamResourceInfo is null. " +
                    "Make sure the resource's Build Action is set to \"Resource\", and not \"Content\" " +
                    "or some other value in the item's property window.");
            }

            // Next, try to load the image into a usable type based on
            // the content type passed in. 
            if (contentType == ResourceLoaderContentType.Text)
            {
                // Read the stream as text
                StreamReader reader = new StreamReader(sri.Stream);
                c = (T)(object)reader.ReadToEnd();
            }
            else if (contentType == ResourceLoaderContentType.Xml)
            {
                // First, read the string in as text
                StreamReader reader = new StreamReader(sri.Stream);
                string text = reader.ReadToEnd();

                // Next, load the string into an XDocument. We're using XDocument
                // now instead of XmlDocument.
                XDocument xdoc = XDocument.Parse(text);
                c = (T)((object)xdoc);
            }
            else if (contentType == ResourceLoaderContentType.Xaml)
            {
                // Create an object (probably a WPF control)
                c = (T)XamlReader.Load(sri.Stream);
            }
            else if (contentType == ResourceLoaderContentType.Image)
            {
                // Return a real image. This image can easily be saved to disk.
                System.Drawing.Image i = System.Drawing.Image.FromStream(sri.Stream);
                c = (T)((object)i);
            }
            else if (contentType == ResourceLoaderContentType.ImageControl)
            {
                // Returns a System.Windows.Controls.Image. This is a control used 
                // by WPF to display an image - not an actual image. This control
                // cannot easily be used to save an image to disk. 
                BitmapImage bi = new BitmapImage();
                bi.BeginInit();
                bi.StreamSource = sri.Stream;
                bi.EndInit();
                if (typeof(T) == typeof(ImageSource))
                {
                    c = (T)((object)bi);
                }
                else if (typeof(T) == typeof(Image))
                {
                    Image img = new Image();
                    img.Source = bi;
                    c = (T)((object)img);
                }
            }
            else if (contentType == ResourceLoaderContentType.MemoryStream)
            {
                // Copy the stream into a System.IO.MemoryStream.
                // http://stackoverflow.com/questions/3212707/how-to-get-a-memorystream-from-a-stream-in-net
                MemoryStream memStream = new MemoryStream();
                sri.Stream.CopyTo(memStream);

                c = (T)((object)memStream);
            }

            sri.Stream.Close();
            sri.Stream.Dispose();

            return c;
        }

        /// <summary>
        /// <para>
        /// Attempts to load a content resource from the currently running
        /// application. Your resource must be marked as Build Action = Content
        /// and Copy to Output Directory = Copy Always/Copy if newer.
        /// </para>
        /// <para>
        /// Content files are not embedded into the assembly, so you MUST mark
        /// them to be copied to the output directory. They will appear in
        /// /bin/debug/ if they were copied successfully, so check there for
        /// debugging.
        /// </para>
        /// <para>
        /// For now, the only type of content I've added code to load here
        /// is strings. I can add logic for images, etc in the future, but
        /// I doubt "Content" resources will be used much.
        /// </para>
        /// </summary>
        /// <param name="uri"></param>
        /// <returns>
        /// The loaded resource, as a string.
        /// </returns>
        public static string LoadContent(Uri uri)
        {
            // Load the content
            StreamResourceInfo sri = Application.GetContentStream(uri);
            StreamReader reader = new StreamReader(sri.Stream);
            string result = reader.ReadToEnd();

            // Close the stream
            sri.Stream.Close();
            sri.Stream.Dispose();

            return result;
        }
    }
}
