﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ResourceLoaderContentType.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Sector12Common.ResourceLoaders
{
    using System;
    using System.Linq;

    /// <summary>
    /// Public enum definition for ResourceLoaderContentType.cs.
    /// </summary>
    public enum ResourceLoaderContentType
    {
        /// <summary>
        /// Returns a string. The resource is loaded using StreamReader.ReadToEnd().
        /// </summary>
        Text,

        /// <summary>
        /// Returns an XDocument. The resource is loaded as a string using 
        /// StreamReader.ReadToEnd(), and then loaded into an XDocument.
        /// </summary>
        Xml,

        /// <summary>
        /// <para>
        /// Returns an object, presumably a WPF control. The object will be created
        /// using XamlReader.Load().
        /// </para>
        /// <para>
        /// In order to successfully load a xaml control, you must include the
        /// namespace in your file. For example:
        /// <TextBlock xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation">Testing xaml loader.</TextBlock>
        /// </para>
        /// </summary>
        Xaml,

        /// <summary>
        /// Returns a System.Drawing.Image.
        /// </summary>
        Image,

        /// <summary>
        /// Returns a System.Windows.Controls.Image. This is a control used 
        /// by WPF to display an image - not an actual image.
        /// </summary>
        ImageControl,

        /// <summary>
        /// Returns a System.IO.MemoryStream. This can be used to load virtually
        /// any kind of file.
        /// </summary>
        MemoryStream
    }
}
