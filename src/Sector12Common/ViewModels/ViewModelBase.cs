﻿// -----------------------------------------------------------------------
// <copyright file="ViewModelBase.cs" company="Sector12">
//     Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Sector12Common.ViewModels
{
    using System;
    using System.ComponentModel;
    using System.Linq;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Threading;

    /// <summary>
    /// The Sector12 base view model, that all view models in all projects should
    /// inherit from.
    /// </summary>
    public abstract class ViewModelBase : INotifyPropertyChanged
    {
        /// <summary>
        /// The PropertyChanged event for our current WPF application.
        /// </summary>
        // ReSharper disable once EventNeverSubscribedTo.Global
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Create the OnPropertyChanged method to raise the event.
        /// </summary>
        /// <param name="name"></param>
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        /// <summary>
        /// Refreshes the state of all WPF commands, on the given UIDispatcher.
        /// If the dispatcher provided is anything other than the primary
        /// UIDispatcher, an error will be thown.
        /// </summary>
        /// <param name="uiDispatcher"></param>
        protected void RefreshCommands(Dispatcher uiDispatcher)
        {
            // Make sure to call it on the UI thread, or it won't work.
            uiDispatcher.Invoke(
                DispatcherPriority.Normal,
                (Action)CommandManager.InvalidateRequerySuggested);
        }

        /// <summary>
        /// Executes the provided delegate on the UIDispatcher thread. The code
        /// used to call this function is pretty damn sexy and compact. Check out
        /// these discussions if you want to read more about how to call delegates
        /// using lamda expressions and anonymous functions.
        /// http://stackoverflow.com/questions/4621623/wpf-multithreading-ui-dispatcher-in-mvvm
        /// http://stackoverflow.com/questions/2082615/pass-method-as-parameter-using-c-sharp
        /// </summary>
        /// <param name="delegateAction"></param>
        protected void UI(Action delegateAction)
        {
            // Application.Current.Dispatcher is always the UIDispatcher
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, delegateAction);
        }
    }
}
