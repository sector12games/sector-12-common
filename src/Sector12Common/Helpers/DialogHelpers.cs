﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DialogHelpers.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Sector12Common.Helpers
{
    using System;
    using System.Linq;

    /// <summary>
    /// Class definition for DialogHelpers.cs.
    /// </summary>
    public static class DialogHelpers
    {
        /// <summary>
        /// Allows the user to choose a directory via a popup dialog.
        /// </summary>
        /// <param name="startingLocation"></param>
        /// <param name="owner"></param>
        /// <returns>
        /// The path to the directory chosen.
        /// </returns>
        public static string ChooseDirectory(string startingLocation, System.Windows.Window owner)
        {
            // Display a dialog for the user to pick a save location (a folder).
            // Make sure the dialog is centered when shown.
            System.Windows.Forms.FolderBrowserDialog d = new System.Windows.Forms.FolderBrowserDialog();
            d.RootFolder = Environment.SpecialFolder.MyComputer;
            d.ShowNewFolderButton = false;

            // So many bugs with the folder dialog browser. Sigh. So I've discovered
            // a pretty nasty bug where if the starting location is a folder that is
            // on the user's desktop, you can ONLY select that folder. The deskotp
            // folder does not expand, and does not have any sub-folders. Cleary, this
            // completely breaks the purpose of the folder select dialog if you can
            // only select one control. This bug does not seem to occur when setting
            // the starting value to other locations on the file system (c:\ is fine).
            // The issue occurs regardless of whether the dialog's "RootFolder" is 
            // set to SpecialFolder.DesktopDirectory (real folder) or 
            // SpecialFolder.Desktop (virtual directory). If I set the "RootFolder" to
            // SpecialFolder.MyComputer, it works fine initially (because MyComputer
            // ignores starting location and uses it's own value after first use),
            // but it breaks the second time I try to use it. For these reasons,
            // I've opted to never set the starting location. I create a new dialog
            // every time, always set to MyComputer, so that it will always work
            // (because it's a new dialog every time, MyComputer will always be on
            // it's first use, and will never break). Sadly, this means that I won't
            // be able to scroll to the already selected value. However, I still save
            // previously used values in the WPF Application Settings, so it's not a 
            // total loss. 
            // 
            // This bug seems to be an issue that does not happen on every machine, 
            // and does not even exist in every project. Some users reported it worked 
            // in one project and failed in another on the same machine. For more 
            // details, see this discussion:
            // http://stackoverflow.com/questions/9973083/bizarre-folderbrowserdialog-behaviour
            // 
            // Try to default the open folder dialog to the location currently
            // specified in the worldDirectory text box. There is what appears
            // to be a bug on SOME versions of windows, and it's not consistent
            // across all computers. The open file dialog does not scroll to the
            // "SelectedPath" value. Read more about this bug here:
            // http://stackoverflow.com/questions/6942150/why-folderbrowserdialog-dialog-does-not-scroll-to-selected-folder
            //if (FileSystemHelper.IsDirectoryValid(startingLocation))
            //    d.SelectedPath = startingLocation;

            // Display the dialog
            MessageBoxHelper.PrepToCenterMessageBoxOnForm(owner);
            System.Windows.Forms.DialogResult result = d.ShowDialog();

            // Update our textbox to display the file location the user
            // just selected in the popup dialog.
            if (result == System.Windows.Forms.DialogResult.OK)
                return d.SelectedPath;
            else
                return startingLocation;
        }
    }
}
