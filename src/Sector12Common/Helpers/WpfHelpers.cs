﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WpfHelpers.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Sector12Common.Helpers
{
    using System;
    using System.Linq;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Class definition for WpfHelpers.cs.
    /// </summary>
    public static class WpfHelpers
    {
        /// <summary>
        /// Finds a Child of a given item in the visual tree. 
        /// </summary>
        /// <typeparam name="T">The type of the queried item.</typeparam>
        /// <param name="parent">A direct parent of the queried item.</param>
        /// <returns>
        /// The first child item that matches the submitted type parameter. 
        /// If no matching item can be found, a null parent is returned.
        /// </returns>
        public static T TryFindChild<T>(DependencyObject parent)
           where T : DependencyObject
        {
            if (parent == null)
                return null;

            T foundChild = null;

            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);

                // If the child is not of the request child type child
                T childType = child as T;
                if (childType == null)
                {
                    // Recursively drill down the tree
                    foundChild = TryFindChild<T>(child);

                    // If the child is found, break so we do not overwrite the found child. 
                    if (foundChild != null)
                        break;
                }
                else
                {
                    // Child element found.
                    foundChild = (T)child;
                    break;
                }
            }

            return foundChild;
        }

        /// <summary>
        /// Finds a parent of the given type in the visual tree.
        /// </summary>
        /// <typeparam name="T">The type of the item to find.</typeparam>
        /// <param name="child">A direct child of the item to find.</param>
        /// <returns>
        /// The first parent item that matches the submitted type parameter. 
        /// If no matching item can be found, null is returned.
        /// </returns>
        public static T TryFindParent<T>(DependencyObject child)
            where T : DependencyObject
        {
            if (child == null)
                return null;

            // Try to convert the parent into the specified type
            var parent = VisualTreeHelper.GetParent(child);
            T parentType = parent as T;

            // Recursively check the next parent if necessary
            if (parentType == null)
                return TryFindParent<T>(parent);
            else
                return parentType;
        }
    }
}
