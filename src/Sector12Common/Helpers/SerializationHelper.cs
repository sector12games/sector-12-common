﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SerializationHelper.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Sector12Common.Helpers
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Class definition for SerializationHelper.cs.
    /// </summary>
    public static class SerializationHelper
    {
        /// <summary>
        /// Serializes an object to disk using the XmlSerializer.
        /// </summary>
        /// <typeparam name="T">The type of the object to serialize.</typeparam>
        /// <param name="serializableObject"></param>
        /// <param name="absolutePath"></param>
        /// <param name="overwrite"></param>
        public static void SerializeObjectToFile<T>(
            T serializableObject, 
            string absolutePath, 
            bool overwrite) where T : class
        {
            if (serializableObject == null)
                return;

            // Create our serializer
            XmlDocument xmlDocument = new XmlDocument();
            XmlSerializer serializer = new XmlSerializer(serializableObject.GetType());

            // By default, the xml serializer will overwrite any existing files
            // when it attempts to save. In order to prevent automatic overwriting,
            // we need to change the file name manually.
            if (!overwrite)
                absolutePath = FileSystemHelper.GetAlternateFileNameToAvoidOverwrite(absolutePath);

            // Now serialize the object and save to disk
            using (MemoryStream stream = new MemoryStream())
            {
                serializer.Serialize(stream, serializableObject);
                stream.Position = 0;
                xmlDocument.Load(stream);
                xmlDocument.Save(absolutePath);
                stream.Close();
            }
        }

        /// <summary>
        /// Deserializes an xml file into an object list
        /// </summary>
        /// <typeparam name="T">The type of the object.</typeparam>
        /// <param name="absoluteFileName"></param>
        /// <returns>
        /// The deserialized object.
        /// </returns>
        public static T DeSerializeObjectFromFile<T>(string absoluteFileName)
        {
            if (string.IsNullOrEmpty(absoluteFileName)) 
            { 
                return default(T); 
            }

            T objectOut;

            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(absoluteFileName);
            string xmlString = xmlDocument.OuterXml;

            using (StringReader read = new StringReader(xmlString))
            {
                Type outType = typeof(T);

                XmlSerializer serializer = new XmlSerializer(outType);
                using (XmlReader reader = new XmlTextReader(read))
                {
                    objectOut = (T)serializer.Deserialize(reader);
                    reader.Close();
                }

                read.Close();
            }

            return objectOut;
        }

        /// <summary>
        /// Serializes an object to a string, using an XmlSerializer.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>The xml-serialized string.</returns>
        public static string SerializeObjectToString(object obj)
        {
            XmlSerializer serializer = new XmlSerializer(obj.GetType());

            using (StringWriter writer = new StringWriter())
            {
                serializer.Serialize(writer, obj);

                return writer.ToString();
            }
        }
    }
}
