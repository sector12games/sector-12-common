﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExceptionHelpers.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Sector12Common.Helpers
{
    using System;
    using System.ComponentModel;
    using System.Linq;

    /// <summary>
    /// Class definition for ExceptionHelpers.cs.
    /// </summary>
    public static class ExceptionHelpers
    {
        /// <summary>
        /// <para>
        /// Attempts to retrieve the windows error code of the given exception.
        /// For info on the algoritm, check out the stack overflow discussion.
        /// For a full list of windows error codes, check out the msdn link.
        /// </para>
        /// <para>
        /// http://stackoverflow.com/questions/6893165/how-to-get-exception-error-code-in-c-sharp
        /// http://msdn.microsoft.com/en-us/library/ms681381%28VS.85%29.aspx
        /// </para>
        /// </summary>
        /// <param name="e"></param>
        /// <returns>
        /// The windows eror code.
        /// </returns>
        public static int GetErrorCode(Exception e)
        {
            // By default, we'll return -1 if we can't determine the
            // real access code.
            int code = -1;

            // Try to convert the exception to a Win32Exception
            Win32Exception w32Ex = e as Win32Exception;
            if (w32Ex == null)
                w32Ex = e.InnerException as Win32Exception;

            // Determine the code if possible
            if (w32Ex != null)
                code = w32Ex.ErrorCode;

            return code;
        }
    }
}
