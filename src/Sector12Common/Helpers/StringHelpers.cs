﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StringHelpers.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Sector12Common.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Class definition for StringHelpers.cs.
    /// </summary>
    public static class StringHelpers
    {
        /// <summary>
        /// Breaks a sentence down into segments with a maximum length. Individual 
        /// words will not be broken up into pieces.
        /// </summary>
        /// <param name="sentence"></param>
        /// <param name="maxSegmentLength"></param>
        /// <returns>
        /// The list of string segments.
        /// </returns>
        public static List<string> GetStringSegments(string sentence, int maxSegmentLength)
        {
            List<string> segments = new List<string>();

            // Split the sentence up into words.
            string[] words = sentence.Split(' ');

            // Iterate over all of our words, and build our segments. Each segment
            // can only be, at a maximum, as long as the maxSegmentLength variable
            // that was passed in.
            string currentSegment = "";
            // ReSharper disable once ForCanBeConvertedToForeach
            for (int i = 0; i < words.Length; i++)
            {
                // If the word is short enough to add to the current segment, add it.
                // Otherwise, add the current segment to the list of segments and then
                // start a new one.
                if (currentSegment.Length + words[i].Length <= maxSegmentLength)
                {
                    currentSegment += words[i] + " ";
                }
                else
                {
                    segments.Add(currentSegment);
                    currentSegment = words[i] + " ";
                }
            }

            // Add the last segment
            segments.Add(currentSegment);

            return segments;
        }
    }
}
