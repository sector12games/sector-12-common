﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FileSystemHelper.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Sector12Common.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading;

    /// <summary>
    /// Class definition for FileSystemHelper.cs.
    /// </summary>
    public static class FileSystemHelper
    {
        /// <summary>
        /// Checks to see if the directory specified actually exists. 
        /// Directories in windows apparently cannot be marked as read-only. 
        /// Only the files within the directory are marked as read-only. 
        /// Thus, we'll never have write permission issues when saving 
        /// files to this directory.
        /// </summary>
        /// <param name="path"></param>
        /// <returns>
        /// Whether or not the path specified is a valid directory.
        /// </returns>
        public static bool IsDirectoryValid(string path)
        {
            // Check to make sure the user entered a location
            if (string.IsNullOrWhiteSpace(path))
                return false;

            // Check to see if the directory exists
            if (!Directory.Exists(path))
                return false;

            return true;
        }

        /// <summary>
        /// Calculate the total size of a directory and all of it's subdirectories
        /// in bytes. This was stolen directly from msdn, so as far as I know this
        /// is the best way to do this at the moment.
        /// http://msdn.microsoft.com/en-us/library/system.io.directory.aspx
        /// </summary>
        /// <param name="d"></param>
        /// <returns>
        /// The size of the directory, in bytes.
        /// </returns>
        public static long DirSize(DirectoryInfo d)
        {
            long size = 0;

            if (!d.Exists)
                return size;

            // Add file sizes
            FileInfo[] fis = d.GetFiles();
            foreach (FileInfo fi in fis)
            {
                size += fi.Length;
            }

            // Add subdirectory sizes
            DirectoryInfo[] dis = d.GetDirectories();
            foreach (DirectoryInfo di in dis)
            {
                size += DirSize(di);
            }

            return size;
        }

        /// <summary>
        /// Creates a nicely formatted string when given a number of bytes.
        /// This is used to represent data size, so it will convert bytes
        /// into GB, MB, KB. If it is less than one KB, we won't care about
        /// it. There is probably a better, built in way to do this, but
        /// I couldn't find one.
        /// </summary>
        /// <param name="b">
        /// The value passed in will be a double because our result string 
        /// needs to be a fraction. Longs can be passed in because C# will 
        /// automatically cast from long to double.
        /// </param>
        /// <returns>
        /// The size of the directory, as a nicely formatted string.
        /// </returns>
        public static string FormatDataSizeString(double b)
        {
            // Remember that 1KB = 1024B, not 1000. Use doubles here,
            // not longs, because longs do not support fractions.
            const double GB = 1073741824;
            const double MB = 1048576;
            const double KB = 1024;
            double result;
            string resultString;

            // Round to the nearest decimal place for GB and MB,
            // but chop off the decimal for KB. We don't care about
            // numbers that small.
            if (b > GB)
            {
                result = b / 1024 / 1024 / 1024;
                result = Math.Round(result, 1);
                resultString = result + " GB";
            }
            else if (b > MB)
            {
                result = b / 1024 / 1024;
                result = Math.Round(result, 1);
                resultString = result + " MB";
            }
            else if (b > KB)
            {
                result = b / 1024;
                result = Math.Round(result);
                resultString = result + " KB";
            }
            else
            {
                // 0 KB is the lowest result that we will return.
                resultString = "0 KB";
            }

            return resultString;
        }

        /// <summary>
        /// Adds a number to the end of a filename if the file already exists.
        /// This works in the same way as the windows desktop. If the file does
        /// not already exist, the original filename will be used.
        /// </summary>
        /// <param name="absolutePath"></param>
        /// <returns>
        /// A safe filename.
        /// </returns>
        public static string GetAlternateFileNameToAvoidOverwrite(string absolutePath)
        {
            // Break down the path into pieces
            string nameWithoutExtension = Path.GetFileNameWithoutExtension(absolutePath);
            string directoryName = Path.GetDirectoryName(absolutePath);
            string extension = Path.GetExtension(absolutePath);

            // Add a counter in parentheses to the end of the file if it 
            // already exists. This is the same behavior as the Windows desktop.
            string newPath = absolutePath;
            int counter = 1;
            while (File.Exists(newPath))
            {
                newPath = directoryName + "\\" + nameWithoutExtension + "(" + counter + ")" + extension;
                counter++;
            }

            // We now have a filename that won't overwrite anything
            return newPath;
        }

        /// <summary>
        /// Adds a number to the end of a directory if the directory already exists.
        /// This works in the same way as the windows desktop. If the directory does
        /// not already exist, the original name will be used.
        /// </summary>
        /// <param name="absolutePath"></param>
        /// <returns>
        /// A safe directory name.
        /// </returns>
        public static string GetAlternateDirectoryNameToAvoidOverwrite(string absolutePath)
        {
            // Add a counter in parentheses to the end of the file if it 
            // already exists. This is the same behavior as the Windows desktop.
            int counter = 1;
            string newPath = absolutePath;
            while (Directory.Exists(newPath))
            {
                newPath = absolutePath + "(" + counter + ")";
                counter++;
            }

            return newPath;
        }

        /// <summary>
        /// Gets a timestamp used for saving files.
        /// </summary>
        /// <returns>The timestamp.</returns>
        public static string GetFileTimeStamp()
        {
            string timeStamp = DateTime.Now.ToString("yyyy-MM-dd-HH.mm*ss");
            timeStamp = timeStamp.Replace(".", "h");
            timeStamp = timeStamp.Replace("*", "m");
            timeStamp += "s";

            return timeStamp;
        }

        /// <summary>
        /// Creates a new archive folder and moves the directory into it. The name
        /// of the new archive folder is just a timestamp (eg. 2013-08-06). If an
        /// archiveLocation is specified, the new timestamp directory will be placed
        /// inside of it. Otherwise, the new timestamp directory will be created
        /// in the same directory in which the directory we want to archive currently
        /// resides.
        /// </summary>
        /// <param name="dir">
        /// The directory to archive.
        /// </param>
        /// <param name="archiveLocation">
        /// You can optionally specify the folder that you'd like the timestamped
        /// folder to be created within. This would typically be something along
        /// the line of an "Arhives" folder. If this is not specified, the timestamped
        /// folder will be created in the same location as the directory we want
        /// to archive.
        /// </param>
        /// <param name="timestamp">
        /// <para>
        /// You can optionally provide your own timestamp instead of having this
        /// method create it's own. By using this optional parameter, you can
        /// archive multiple directories into a single timestamp folder.
        /// </para>
        /// <para>
        /// This also allows you to archive the folder based on the original
        /// creation date of the folder (time of creation), instead of the current 
        /// time (time of archiving).
        /// </para>
        /// </param>
        /// <param name="msTimeout">The timeout period, in milliseconds.</param>
        /// <returns>
        /// The timestamped archive folder that our dir was moved in to.
        /// </returns>
        public static DirectoryInfo ArchiveDirectoryWithTimestamp(
            DirectoryInfo dir, 
            DirectoryInfo archiveLocation = null, 
            string timestamp = null, 
            int msTimeout = 10000)
        {
            // If the directory does not exist, we can't do anything
            if (dir == null || !dir.Exists)
                return null;

            // Variables we will use later
            DirectoryInfo timestampFolder;
            string originalLocation = dir.FullName;
            int timeSpentSleeping = 0;

            // Determine the timestamp that we'll be using as the name of our folder.
            // The timestamp is our current time (the time of archiving).
            string finalTimeStamp;
            if (!string.IsNullOrEmpty(timestamp))
                finalTimeStamp = timestamp;
            else
                finalTimeStamp = GetFileTimeStamp();

            // Create the new timestamp directory. If an archiveLocation is specified,
            // the timestamp folder will be created there. Otherwise, it will be 
            // created in the same location as the directory we want to archive.
            // If the directory already exists, nothing will happen. No error will 
            // be thrown.
            if (archiveLocation != null)
                timestampFolder = archiveLocation.CreateSubdirectory(finalTimeStamp);
            else
                timestampFolder = dir.Parent.CreateSubdirectory(finalTimeStamp);

            // If we've already run a previous build, and we run another one
            // quickly after the first one stops, it's possible we'll get an
            // "Access Denied" exception. This will typically only happen
            // in testing, but it's still a good issue to avoid.
            bool moveSuccessful = false;
            while (!moveSuccessful)
            {
                try
                {
                    // Try to move the directory. If we get an "Access Denied"
                    // exception, wait a little bit and try moving it again.
                    dir.MoveTo(timestampFolder.FullName + "\\" + dir.Name);
                    moveSuccessful = true;

                    // Finally, wait until the directory is really moved.
                    // This is to fix a Windows lag issue I came across.
                    WaitUntilDirectoryDoesNotExist(originalLocation);
                }
                catch (IOException)
                {
                    Thread.Sleep(200);
                    timeSpentSleeping += 200;

                    // Stop trying and throw the exception if we have exceeded
                    // our timeout period.
                    if (timeSpentSleeping > msTimeout)
                        throw;
                }
            }

            // Return the final, timestamped folder in which our directory
            // was archived.
            return timestampFolder;
        }

        /// <summary>
        /// Repeatedly calls ArchiveDirectoryWithTimestamp() for each directory in
        /// the list. All directories will be archived in the same timestamped
        /// folder.
        /// </summary>
        /// <param name="dirs">
        /// The list of directories to archive.
        /// </param>
        /// <param name="archiveLocation">
        /// You can optionally specify the folder that you'd like the timestamped
        /// folder to be created within. This would typically be something along
        /// the line of an "Arhives" folder. If this is not specified, the timestamped
        /// folder will be created in the same location as the directory we want
        /// to archive.
        /// </param>
        /// <returns>
        /// The timestamped archive folder that our dir was moved in to.
        /// </returns>
        public static DirectoryInfo ArchiveDirectoriesWithTimestamp(
            IEnumerable<DirectoryInfo> dirs, 
            DirectoryInfo archiveLocation = null)
        {
            // The first item in the list will be archived without specifying
            // a timestamp - the method will create on for us. Each item after
            // the first will use the timestamp the first item received. This
            // way, all directories in our list will be archived under the
            // same time-stamped folder.
            DirectoryInfo timestampFolder = null;
            foreach (DirectoryInfo dir in dirs)
            {   
                if (timestampFolder == null)
                    timestampFolder = ArchiveDirectoryWithTimestamp(dir, archiveLocation);
                else
                    timestampFolder = ArchiveDirectoryWithTimestamp(dir, archiveLocation, timestampFolder.Name);
            }

            // The timestamp folder will be the same for all dirs in our list.
            // They will all have been moved to the same location.
            return timestampFolder;
        }

        /// <summary>
        /// <para>
        /// Copies a directory's contents to another location.
        /// </para>
        /// <para>
        /// Taken directly from here:
        /// http://msdn.microsoft.com/en-us/library/bb762914.aspx
        /// </para>
        /// </summary>
        /// <param name="sourceDirName"></param>
        /// <param name="destDirName"></param>
        /// <param name="copySubDirs"></param>
        public static void CopyDirectory(string sourceDirName, string destDirName, bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);
            DirectoryInfo[] dirs = dir.GetDirectories();

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            // If the destination directory doesn't exist, create it. 
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, false);
            }

            // If copying subdirectories, copy them and their contents to new location. 
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    CopyDirectory(subdir.FullName, temppath, true);
                }
            }
        }

        /// <summary>
        /// <para>
        /// This method will attempt to delete a directory, and if an 
        /// "Access Denied" exception is thrown, will wait a short period
        /// of time and try again. You can specify an optional timeout
        /// period, at which point we will stop retrying and the IOExecption 
        /// will be thrown. The default is 10 seconds.
        /// </para>
        /// <para>
        /// If we've already run a previous build, and we run another one
        /// quickly after the first one stops, it's possible we'll get an
        /// "Access Denied" exception. This will typically only happen 
        /// in testing, but it's still a good issue to avoid.
        /// </para>
        /// </summary>
        /// <param name="dir"></param>
        /// <param name="msTimeout"></param>
        public static void DeleteDirectoryWaitForAccess(
            DirectoryInfo dir, 
            int msTimeout = 10000)
        {
            // If the directory does not exist, we can't do anything
            if (dir == null || !dir.Exists)
                return;

            bool deleteSuccessful = false;
            int timeSpentSleeping = 0;

            while (!deleteSuccessful)
            {
                try
                {
                    // Try to delete the directory. If we get an "Access Denied"
                    // exception, wait a little bit and try deleting it again.
                    dir.Delete(true);

                    // Break the loop
                    deleteSuccessful = true;

                    // Finally, wait until the directory is really deleted.
                    // This is to fix a Windows lag issue I came across.
                    WaitUntilDirectoryDoesNotExist(dir);
                }
                catch (IOException)
                {
                    Thread.Sleep(200);
                    timeSpentSleeping += 200;

                    // Stop trying and throw the exception if we have exceeded
                    // our timeout period.
                    if (timeSpentSleeping > msTimeout)
                        throw;
                }
            }
        }

        /// <summary>
        /// Repeatedly calls DeleteDirectoryWaitForAccess() for each directory
        /// in our list.
        /// </summary>
        /// <param name="dirs"></param>
        /// <param name="msTimeout"></param>
        public static void DeleteDirectoriesWaitForAccess(
            IEnumerable<DirectoryInfo> dirs, 
            int msTimeout = 10000)
        {
            foreach (DirectoryInfo dir in dirs)
            {
                DeleteDirectoryWaitForAccess(dir, msTimeout);
            }
        }

        /// <summary>
        /// <para>
        /// This method will attempt to delete a file, and if an 
        /// "Access Denied" exception is thrown, will wait a short period
        /// of time and try again. You can specify an optional timeout
        /// period, at which point we will stop retrying and the IOExecption 
        /// will be thrown. The default is 10 seconds.
        /// </para>
        /// <para>
        /// If we've already run a previous build, and we run another one
        /// quickly after the first one stops, it's possible we'll get an
        /// "Access Denied" exception. This will typically only happen 
        /// in testing, but it's still a good issue to avoid.
        /// </para>
        /// </summary>
        /// <param name="file"></param>
        /// <param name="msTimeout"></param>
        public static void DeleteFileWaitForAccess(FileInfo file, int msTimeout = 10000)
        {
            // If the file does not exist, we can't do anything
            if (file == null || !file.Exists)
                return;
            
            bool deleteSuccessful = false;
            int timeSpentSleeping = 0;

            while (!deleteSuccessful)
            {
                try
                {
                    // Try to delete the file. If we get an "Access Denied"
                    // exception, wait a little bit and try deleting it again.
                    file.Delete();
                    deleteSuccessful = true;
                }
                catch (IOException)
                {
                    Thread.Sleep(200);
                    timeSpentSleeping += 200;

                    // Stop trying and throw the exception if we have exceeded
                    // our timeout period.
                    if (timeSpentSleeping > msTimeout)
                        throw;
                }
            }
        }

        /// <summary>
        /// Repeatedly calls DeleteFileWaitForAccess() for each file in our list.
        /// </summary>
        /// <param name="files"></param>
        /// <param name="msTimeout"></param>
        public static void DeleteFilesWaitForAccess(
            IEnumerable<FileInfo> files, 
            int msTimeout = 10000)
        {
            foreach (FileInfo file in files)
            {
                DeleteFileWaitForAccess(file, msTimeout);
            }
        }

        /// <summary>
        /// <para>
        /// Creates a temporary file with the given extension, and writes the contents
        /// of the memory stream to it. 
        /// </para>
        /// <para>
        /// The location of the temporary file will be something similar to:
        /// "C:\Users\Shaun\AppData\Local\Temp\tmp4ACD.xml"
        /// </para>
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="extension"></param>
        /// <returns>
        /// A temporary file with the specified data written to it.
        /// </returns>
        public static FileInfo CreateTemporaryFileFromMemoryStream(
            MemoryStream ms, 
            string extension = ".tmp")
        {
            // Get a unique temporary file name and create a file
            string tempFileName = Path.GetTempFileName();
            FileStream fs = File.Create(tempFileName);

            // Write the contents of the MemoryStream to the file. Close the streams
            // after we are done.
            ms.WriteTo(fs);
            ms.Flush();
            ms.Close();
            fs.Flush();
            fs.Close();

            // By default, temporary files are created using the extension ".tmp". If
            // desired, we can change this extension now.
            FileInfo tempFile = new FileInfo(tempFileName);
            tempFile.MoveTo(Path.ChangeExtension(tempFile.FullName, extension));

            // Finally, we can return the file
            return tempFile;
        }

        /// <summary>
        /// <para>
        /// This method waits until a directory no longer exists. This is to
        /// correct a Windows lag issue that I've occasionally run into.
        /// </para>
        /// <para>
        /// Windows has some weird lag time when deleting directories. In some
        /// of my applications, such as WorldBuilder/FontBuilder, I delete or
        /// move directories (overwriting or archiving) and then quickly create 
        /// fresh, empty directories with the same name. On occasion, windows 
        /// will actually try to create the new directory before the original
        /// directory has been fully deleted. This leads to the directory 
        /// eventually being deleted, but not re-created.
        /// </para>
        /// <para>
        /// To fix this, any time we delete a directory, we should wait until 
        /// it's really gone before we continue execution. I've never come
        /// across this issue with moving/renaming directories, but it seems
        /// likely that a similar issue exists in that case as well. Just to
        /// be safe, we should call this method any time we delete or move
        /// directories.
        /// </para>
        /// </summary>
        /// <param name="dir"></param>
        private static void WaitUntilDirectoryDoesNotExist(DirectoryInfo dir)
        {
            // Wait until we're sure the directory no longer exists. There is
            // occasionally some weird lag time involved, where windows tries
            // to create the directory, but it still hasn't been fully deleted.
            // This leads to the directory eventually being deleted, but not
            // re-created.
            while (dir.Exists)
            {
                Thread.Sleep(200);
                dir.Refresh();
            }
        }

        /// <summary>
        /// <para>
        /// This method waits until a directory no longer exists. This is to
        /// correct a Windows lag issue that I've occasionally run into.
        /// </para>
        /// <para>
        /// Windows has some weird lag time when deleting directories. In some
        /// of my applications, such as WorldBuilder/FontBuilder, I delete or
        /// move directories (overwriting or archiving) and then quickly create 
        /// fresh, empty directories with the same name. On occasion, windows 
        /// will actually try to create the new directory before the original
        /// directory has been fully deleted. This leads to the directory 
        /// eventually being deleted, but not re-created.
        /// </para>
        /// <para>
        /// To fix this, any time we delete a directory, we should wait until 
        /// it's really gone before we continue execution. I've never come
        /// across this issue with moving/renaming directories, but it seems
        /// likely that a similar issue exists in that case as well. Just to
        /// be safe, we should call this method any time we delete or move
        /// directories.
        /// </para>
        /// </summary>
        /// <param name="dirAbsolutePath"></param>
        private static void WaitUntilDirectoryDoesNotExist(string dirAbsolutePath)
        {
            DirectoryInfo d = new DirectoryInfo(dirAbsolutePath);
            WaitUntilDirectoryDoesNotExist(d);
        }
    }
}
