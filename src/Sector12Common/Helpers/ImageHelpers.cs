﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ImageHelpers.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Sector12Common.Helpers
{
    using System;
    using System.Drawing;
    using System.Linq;

    /// <summary>
    /// Class definition for ImageHelpers.cs.
    /// </summary>
    public class ImageHelpers
    {
        /// <summary>
        /// Compares two images for equality by checking every pixel's color. 
        /// If all pixels in both images are exactly the same, returns true.
        /// </summary>
        /// <param name="image1"></param>
        /// <param name="image2"></param>
        /// <returns>True if images are equal.</returns>
        public static bool ImageCompareByPixel(Image image1, Image image2)
        {
            Bitmap b1 = new Bitmap(image1);
            Bitmap b2 = new Bitmap(image2);

            // If the two images have different dimensions, they are obviously
            // not the same.
            if (image1.Width != image2.Width || image1.Height != image2.Height)
                return false;

            // Iterate over each pixel
            for (int i = 0; i < b1.Width; i++)
            {
                for (int j = 0; j < b1.Height; j++)
                {
                    // Get the pixel color at this index
                    Color c1 = b1.GetPixel(i, j);
                    Color c2 = b2.GetPixel(i, j);

                    // Compare the colors. If any pixels are different, the
                    // two images are not equal. Use the .ToArgb() method to
                    // compare ints. If we use c1.Equals(c2), we may be comparing
                    // more than just color data:
                    // http://stackoverflow.com/questions/7464994/how-to-compare-a-color-by-the-getpixel-method-and-a-color-passed-in-a-method-lik
                    if (c1.ToArgb() != c2.ToArgb())
                        return false;
                }
            }

            // Every pixel of both images are the same. They are equal.
            return true;
        }

        ///// <summary>
        ///// <para>
        ///// *** NOTE *** 
        ///// I recently tried this method again, and it failed to return true when both
        ///// images were identical. From reading the guy's blog below, it looks like even
        ///// he wasn't 100% sure that this method would work all of the time, and might fail
        ///// if images were too large or small. For now, I've decided to remove this method
        ///// and force the usage of ImageCompareByPixel(). It's 100% correct, and there's no
        ///// "string conversion magic" going on that I don't fully understand. The string
        ///// compare would be no doubt much faster, but at least for the "world builder"
        ///// project, speed is not important. Accuracy is.
        ///// -- ssb 7/18/2013
        ///// </para>
        ///// <para>
        ///// Compares two images for equality. Each image is converted into a 
        ///// Base64 string and the strings are compared. I stole this code from:
        ///// http://blogs.msdn.com/b/domgreen/archive/2009/09/06/comparing-two-images-in-c.aspx
        ///// </para>
        ///// </summary>
        ///// <param name="firstImage"></param>
        ///// <param name="secondImage"></param>
        ///// <param name="format"></param>
        ///// <returns></returns>
        //public static bool ImageCompareByString(Image firstImage, Image secondImage, ImageFormat format)
        //{
        //    // Save each image to a temporary memory steam and convert it to a
        //    // base64 string for comparison.
        //    MemoryStream ms = new MemoryStream();
        //    firstImage.Save(ms, format);
        //    string firstBitmap = Convert.ToBase64String(ms.ToArray());

        //    ms.Position = 0;
        //    secondImage.Save(ms, format);
        //    string secondBitmap = Convert.ToBase64String(ms.ToArray());

        //    if (firstBitmap.Equals(secondBitmap))
        //        return true;
        //    else
        //        return false;
        //}
    }
}
