﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DateTimeHelpers.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Sector12Common.Helpers
{
    using System;
    using System.Linq;

    /// <summary>
    /// Class definition for DateTimeHelpers.cs.
    /// </summary>
    public static class DateTimeHelpers
    {
        /// <summary>
        /// When displaying a graphical timer, I always want to show the
        /// minutes and seconds, but I don't want to show the hours unless
        /// they are necessary. This isn't possible using a single format
        /// string as far as I can tell, so I have to use two.
        /// Examples:
        /// 00:15       (15 secs - don't show hours)
        /// 10:15       (10 min, 15 secs - don't show hours)
        /// 01:14:22    (1 hour, 14 min, 22 secs)
        /// </summary>
        public const string LessThanOneHourDateFormat = "{0:mm\\:ss}";
        public const string MoreThanOneHourDateFormat = "{0:hh\\:mm\\:ss}";

        /// <summary>
        /// Returns a full timestamp, down to the second.
        /// </summary>
        /// <returns>
        /// The timestamp.
        /// </returns>
        public static string GetTimestamp()
        {
            return DateTime.Now.ToString("yyyyMMddHHmmss");
        }

        /// <summary>
        /// Returns a full timestamp, down to fractions of a second.
        /// </summary>
        /// <returns>
        /// The timestamp.
        /// </returns>
        public static string GetTimestampWithFractions()
        {
            return DateTime.Now.ToString("yyyyMMddHHmmssffff");
        }
    }
}
