﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GifImageTest.xaml.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Sector12CommonTest
{
    using System;
    using System.Linq;
    using System.Windows;

    /// <summary>
    /// Interaction logic for GifImageTest.xaml
    /// </summary>
    public partial class GifImageTest : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GifImageTest"/> class.
        /// </summary>
        public GifImageTest()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Event listener.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Todd_Click(object sender, RoutedEventArgs e)
        {
            GifControl.GifUri = new Uri(@"pack://application:,,,/Content/Images/cock_goblin_left.gif");
        }

        /// <summary>
        /// Event listener.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Larrity_Click(object sender, RoutedEventArgs e)
        {
            GifControl.GifUri = new Uri(@"pack://application:,,,/Content/Images/larrity_dance.gif");
        }

        /// <summary>
        /// Event listener.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Larrity2_Click(object sender, RoutedEventArgs e)
        {
            GifControl.GifUri = new Uri(@"pack://application:,,,/Content/Images/larrity_talking_left.gif");
        }

        /// <summary>
        /// Event listener.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Dave_Click(object sender, RoutedEventArgs e)
        {
            GifControl.GifUri = new Uri(@"pack://application:,,,/Content/Images/dave_humping_left.gif");
        }

        /// <summary>
        /// Event listener.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Jerry_Click(object sender, RoutedEventArgs e)
        {
            GifControl.GifUri = new Uri(@"pack://application:,,,/Content/Images/jerry_humping_left.gif");
        }
    }
}
