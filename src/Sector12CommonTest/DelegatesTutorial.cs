﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DelegatesTutorial.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Sector12CommonTest
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Threading;

    /// <summary>
    /// <para>
    /// This class won't actually do anything. It's not supposed to be
    /// used or run in any way. It's just for explaining delegates,
    /// Func, Action, Action, and anonymous functions, and lambda
    /// expressions.
    /// </para>
    /// <para>
    /// These are all relatively confusing topics, and it's about time
    /// I got all of my ideas and doc down in one place. I wanted to
    /// write a hefty amount of code and examples, which is why this
    /// is in code rather than on my google sites page.
    /// </para>
    /// <para>
    /// Here is one of the best pages I've found for explaining all
    /// of these concepts. Read it first:
    /// http://blogs.msdn.com/b/brunoterkaly/archive/2012/03/02/c-delegates-actions-lambdas-keeping-it-super-simple.aspx
    /// </para>
    /// </summary>
    [SuppressMessage("StyleCop.CSharp.NamingRules", "*", Justification = "Reviewed. Suppression is OK here.")]
    [SuppressMessage("StyleCop.CSharp.LayoutRules", "*", Justification = "Reviewed. Suppression is OK here.")]
    [SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "*", Justification = "Reviewed. Suppression is OK here.")]
    [SuppressMessage("StyleCop.CSharp.OrderingRules", "*", Justification = "Reviewed. Suppression is OK here.")]
    [SuppressMessage("StyleCop.CSharp.ReadabilityRules", "*", Justification = "Reviewed. Suppression is OK here.")]
    [SuppressMessage("StyleCop.CSharp.SpacingRules", "*", Justification = "Reviewed. Suppression is OK here.")]
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "*", Justification = "Reviewed. Suppression is OK here.")]
    // ReSharper Disable All 
    public class DelegatesTutorial
    {
        private delegate void TestVoidDelegate(string s);
        private delegate int TestIntDelegate(int x, int y);
        private delegate void TestNoParametersDelegate();
        private void TestVoidDelegateMethod(string s)
        {
            Console.WriteLine(s);
        }
        private int TestIntDelegateMethod(int x, int y)
        {
            return x + y;
        }
        private void TestNoParametersDelegateMethod()
        {
            Console.WriteLine("No params passed in.");
        }
        private void TestVoidDelegateMethodObject(object o)
        {
            Console.WriteLine(o.ToString());
        }

        public void WhereToStart()
        {
            // Start here. This is one of the best pages I've found for explaining 
            // all of these concepts. Read it first:
            // http://blogs.msdn.com/b/brunoterkaly/archive/2012/03/02/c-delegates-actions-lambdas-keeping-it-super-simple.aspx
        }

        public void WhatIsADelegate()
        {
            // A delegate is a type-safe pointer (wrapper) for a method. Essentially,
            // it is a type that references a method. Once you assign a method to it,
            // you can call just like any other method. It was introduced in C# 1.0.

            // Examples:

            // 1)
            // // Create the delegate. We need an existing method name.
            TestVoidDelegate d = new TestVoidDelegate(TestVoidDelegateMethod);

            // Call the delegate. Just like a normal method. It does not 
            // return a value, because it is void.
            d("test"); 

            // 2)
            // Now call a method 
            TestIntDelegate d2 = new TestIntDelegate(TestIntDelegateMethod);
            int result = d2(3, 4);

            // Documentation:
            // http://msdn.microsoft.com/en-us/library/ms173171(v=vs.80).aspx
        }

        public void WhatAreAnonymousFunctions()
        {
            // Anonymous functions are functions that are created in-line,
            // and aren't explicitly defined in a class somewhere. They can be
            // used anywhere a delegate can be used. In other words, they are
            // an easier-to-write, inline delegate (pointer to a function).
            // They came about in C# version 2.0.

            // Examples:

            // 1)
            // Still creating a delegate, but the method is in-line
            TestVoidDelegate d = delegate(string s) { Console.WriteLine(s); }; 
            d("test");

            // 2)
            TestIntDelegate d2 = delegate(int x, int y) { return x + y; };
            int result = d2(3, 4);

            // Documentation:
            // http://msdn.microsoft.com/en-us/library/bb882516.aspx
        }

        public void WhatAreLamdaExpressions()
        {
            // Lambda expressions are simply shorthand syntactic sugar. They
            // let you easily create anonymous functions. In other words, lambda
            // expressions ARE anonymous functions. They were introduced in
            // C# verions 3.0. Nowadays, we should always use lambda expressions,
            // as they are easier to write, and much sexier than C# 2.0 anonymous
            // functions. 
            
            // Lamda expressions can take in any number of input parameters 
            // (including zero), and support return types. The return type
            // is implied automatically, but can be casted if necessary.

            // Lamda expressions always have input values on the left, and the 
            // expression on the right. The => is a special symbol used in 
            // lamda expressions. 
            // (input parameters) => expression

            // Examples:

            // 1)
            // Look how much cleaner this is. This creates the same anonymous 
            // function as above, but with much less and cleaner code. Awesome.
            // You can also be a bit more verbose if necessary. You usually 
            // need to add the brackets on the right hand side if you're doing
            // anything very complicated.
            TestVoidDelegate d = s => Console.WriteLine(s);
            TestVoidDelegate d2 = (string s) => { Console.WriteLine(s); };

            // 2)
            // A lambda expression will try to imply the types of the parameters
            // automatically. This often works. If it doesn't however, you can
            // just be more verbose with your types and the compiler won't have
            // any problems.
            TestIntDelegate d3 = (x, y) => { return x + y; }; // No problem. Inferred.
            TestIntDelegate d4 = (int x, int y) => { return x + y; }; // More verbose is ok

            // 3)
            // You don't have to specify any parameters if you don't want to.
            // In order to do this, just use empty parenthesis.
            TestNoParametersDelegate d5 = () => { Console.WriteLine("no params passed in"); };
            TestNoParametersDelegate d6 = () => this.TestNoParametersDelegateMethod();

            // Even this works out, because no params are passed into lambda. 
            // I pass the params manually into the method.
            TestNoParametersDelegate d7 = () => this.TestIntDelegateMethod(3, 4); 

            // Documentation:
            // http://msdn.microsoft.com/en-us/library/vstudio/bb397687.aspx
            // http://stackoverflow.com/questions/761236/difference-between-lambda-expressions-and-anonymous-methods-c-sharp
        }

        public void WhatAreActionsAndFuncs()
        {
            // An Action<> is a delegate. So is a Func<>. They were both introduced
            // in .NET 3.5. They are both essentially more syntactic sugar that
            // allow us to do cool things like pass lambda expressions to other
            // methods. The only difference between Action<> and Func<> is that
            // Func<> supports return values, while Action<> does not. Use Func<>
            // if you need return values, use Action<> if you don't. Simple.

            // The main purpose of Action<> and Func<> is to be able to pass delegates
            // as parameters to other functions without having to explicitly define
            // a delegate like we did in the .NET 1.0 days.

            // There are two types of action. One is Action<>, which supports input
            // parameters, the other is Action, which does not support input params.
            // Basically, they are the same, but use the former if you need to pass
            // parameters into you function or lambda expression.

            // *** One thing to be very careful of, is the fact that Actions don't
            // support return types. You can still CALL methods that return values,
            // but the return value is ignored. I've ran into problems with this 
            // when trying to invoke methods on the UIDispatcher thread (WPF) from
            // a worker thread. See my example below for more detail.

            // Examples:

            // 1)
            // Here we can create delegates without having to define them up top,
            // and without having to explicitly define their types. Very cool.
            Action a = () => Console.WriteLine("Actions are cool");
            Action<int, int> a1 = (x, y) => { int result = x + y; };
            Action<int, int> a2 = (x, y) => { TestIntDelegateMethod(x, y); }; // Allowed, but return type ignored
            Action<string> a3 = s => { TestVoidDelegateMethod(s); };
            Action<string, string> a4 = (s1, s2) => { Console.WriteLine(s1 + s2); };

            // 2)
            // Funcs are created and used the same way as Actions<>. They just support
            // return types. The last value in the <> is the return type.
            Func<int, int, int> f1 = (x, y) => { return TestIntDelegateMethod(x, y); }; // returns int
            Func<int, int, string> f2 = (x, y) => { return (x + y).ToString(); }; // returns string

            // 3)
            // As I mentioned earlier, Actions ignore return values. They don't
            // throw errors - they just ignore the return value if one exists.
            // So don't expect to be able to do something like this and get a 
            // value back:
            System.Windows.Threading.Dispatcher UIDispatcher = null; // Assume this was set from within WPF

            // You can create the action just fine, but the return type is ignored.
            Action actionParam = () => { TestIntDelegateMethod(3, 4); };
            object o = UIDispatcher.Invoke(actionParam); // No value returned, even though .Invoke() says it returns object.
            
            // Creating a Func<> will allow the UIDispatcher to return a value.
            Func<object> functionParam = () => { return TestIntDelegateMethod(3, 4); };
            object o2 = UIDispatcher.Invoke(functionParam); // Func will return a value. Don't use Action.

            // Documentation:
            // http://stackoverflow.com/questions/7630538/c-action-func-vs-methods-whats-the-point
            // http://www.blackwasp.co.uk/FuncAction.aspx
            // http://msdn.microsoft.com/en-us/library/system.action.aspx (actions)
            // http://msdn.microsoft.com/en-us/library/bb549151.aspx (funcs)
        }

        public void WhatArePredicates()
        {
            // Predicate is just a special type of Func<>. Func<T, bool>, where T can
            // be any type. A quote taken from msdn says:
            // "A predicate represents the method that defines a set of criteria and 
            // determines whether the specified object meets those criteria."
            
            // So basically, it's a delegate used for special purposes like sorting
            // lists (comparing many objects to many objects, and returning a bool).

            // Overall, you won't see much of this, but if you come across it, don't
            // be confused. Just think of it as a special Func<T, bool>.

            // Documentation:
            // http://stackoverflow.com/questions/4317479/func-vs-action-vs-predicate
            // http://msdn.microsoft.com/en-us/library/bfcke1bz.aspx
        }

        public void WhatAreMethodGroups()
        {
            // http://stackoverflow.com/questions/886822/what-is-a-method-group-in-c
        }

        public void ThreadAndThreadStart()
        {
            // System.Threading.Thread can be used to spin off another thread that
            // runs in the background. In order to start the thread, you need to
            // pass it a delegate. The delegate you must hand it is special. Here is
            // a good discussion to read as you are going through the example below:
            // http://stackoverflow.com/questions/1195896/c-sharp-threadstart-with-parameters

            // 1) 
            // The first thing to understand is that the Thread() constructor takes
            // one of two arguments:
            //   - ThreadStart (a delegate named ThreadStart that takes no arguments)
            //   - ParameterizedThreadStart (a delegate named ParameterizedThreadStart 
            //                               that takes a single "object" argument).
            Thread validWorker1 = new Thread(new ThreadStart(TestNoParametersDelegateMethod));
            validWorker1.Start();

            Thread validWorker2 = new Thread(new ParameterizedThreadStart(TestVoidDelegateMethodObject));
            object myObject = null;
            validWorker2.Start(myObject);

            // 2)
            // The second thing to understand is that you HAVE to pass it either a 
            // ThreadStart or a ParameterizedThreadStart delegate. You cannot pass
            // it an Action, Action<>, or Func<>, despite the fact that they are 
            // basically the same things. They have different names, and the Thread 
            // code is old, so you have to abide by its old rules. You CAN, however,
            // create a new ThreadStart delegate and pass it parameters manually.
            string myNonsenseString = "blah blah blah";
            ThreadStart validThreadStartDelegate1 = delegate() { Console.WriteLine(myNonsenseString); };
            Thread validWorker3 = new Thread(validThreadStartDelegate1);
            validWorker3.Start();

            ThreadStart validThreadStartDelegate2 = delegate() { TestIntDelegateMethod(2, 7); };
            Thread validWorker4 = new Thread(validThreadStartDelegate2);
            validWorker4.Start();

            // 3)
            // Because you can create a new ThreadStart delegate manually, and pass
            // it one or more parameters manually, you can also pass in a lambda 
            // expression. Lambda expressions are interpretted by the compiler. In
            // other words, their type is not specific - it has to be determined
            // based on the context. Take a look at the examples below. Both are
            // exactly the same, and both work. But the lambda expressions are being
            // converted into two different delegate types. For a more detailed
            // discussion about type coercion and lambdas work, check out:
            // http://stackoverflow.com/questions/3822349/how-do-delegate-lambda-typing-and-coercion-work
            ThreadStart inferredDelegate1 = () => Console.WriteLine("blah");
            Action inferredDelegate2 = () => Console.WriteLine("blah");

            // 4)
            // Method groups can also be used in the same way, because the actual
            // method/parameters combination to use is inferred as well. 
            ThreadStart inferredDelegate3 = TestNoParametersDelegateMethod;
            ParameterizedThreadStart inferredDelegate4 = TestVoidDelegateMethodObject;

            // 5)
            // However, when using both lambda expressions and Method Groups, the 
            // inferred types must match. The examples below will not work, because
            // the methods take different parameters than the delegates were expecting.
                 // ThreadStart invalidDelegate1 = TestVoidDelegateMethodObject;
                 // ParameterizedThreadStart invalidDelegate2 = TestNoParametersDelegateMethod;

                 // ThreadStart invalidDelegate3 = (object o) => TestVoidDelegateMethodObject(o);
                 // ParameterizedThreadStart invalidDelegate4 = () => TestNoParametersDelegateMethod();

            // 6)
            // In conclusion, if I want to pass one or more parameters to the new
            // Thread that I am starting, the most elegant way to do it is to use
            // Lambda expressions. Method Groups are ok for no parameters (implicit
            // conversion to ThreadStart) or a single parameter (implicit conversion
            // to ParameterizedThreadStart), but cannot be used for passing more.
            Thread validWorker5 = new Thread(() => TestIntDelegateMethod(2, 7));
            validWorker5.Start();
        }
    }

    // ReSharper Restore All
}
