﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LinkButtonTest.xaml.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Sector12CommonTest
{
    using System;
    using System.Linq;
    using System.Windows;

    /// <summary>
    /// Interaction logic for LinkButtonTest.xaml
    /// </summary>
    public partial class LinkButtonTest : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LinkButtonTest"/> class.
        /// </summary>
        public LinkButtonTest()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Event listener.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("clicked");
        }
    }
}
