﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FakeTreeViewTest.xaml.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Sector12CommonTest
{
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows;
    using Sector12CommonTest.DataModels;

    /// <summary>
    /// Interaction logic for FakeTreeViewTest.xaml
    /// </summary>
    public partial class FakeTreeViewTest : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FakeTreeViewTest"/> class.
        /// </summary>
        public FakeTreeViewTest()
        {
            Directories = new ObservableCollection<DirectoryDataModel>();
            Directories.Add(new DirectoryDataModel("C:\\", 0));

            InitializeComponent();
        }

        /// <summary>
        /// Gets or sets the list of all directories.
        /// </summary>
        public ObservableCollection<DirectoryDataModel> Directories { get; set; }

        /// <summary>
        /// A safe time to apply the data context.
        /// </summary>
        public override void OnApplyTemplate()
        {
            DirectoryTreeView.DataContext = this;
        }
    }
}
