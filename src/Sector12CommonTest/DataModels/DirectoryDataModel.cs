﻿// -----------------------------------------------------------------------
// <copyright file="DirectoryDataModel.cs" company="Sector12">
//     Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Sector12CommonTest.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Sector12Common.Controls;

    /// <summary>
    /// Class definition for DirectoryDataModel.cs
    /// </summary>
    public class DirectoryDataModel : FakeTreeViewItem
    {
        private DirectoryInfo _directory;
        private FileInfo _file;
        private int _treeLevel;

        /// <summary>
        /// Initializes a new instance of the <see cref="DirectoryDataModel"/> class.
        /// </summary>
        /// <param name="absolutePath"></param>
        /// <param name="treeLevel"></param>
        public DirectoryDataModel(string absolutePath, int treeLevel)
        {
            if (Directory.Exists(absolutePath))
                _directory = new DirectoryInfo(absolutePath);
            else if (File.Exists(absolutePath))
                _file = new FileInfo(absolutePath);

            _treeLevel = treeLevel;
        }

        /// <summary>
        /// Gets or sets the eror message for this node (if one exists).
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Gets the name of this node.
        /// </summary>
        public string Name
        {
            get
            {
                if (_directory != null)
                    return _directory.Name;
                else if (_file != null)
                    return _file.Name;
                else
                    return "Error";
            }
        }

        /// <summary>
        /// Gets the tree level for this node (directory depth).
        /// </summary>
        public override int TreeLevel
        {
            get
            {
                return _treeLevel;
            }
        }

        /// <summary>
        /// Gets a value indicating whether or not the node has children.
        /// </summary>
        public override bool HasChildren
        {
            get
            {
                if (_directory == null)
                    return false;

                try
                {
                    return _directory.GetFileSystemInfos("*", SearchOption.TopDirectoryOnly).Any();
                }
                catch (UnauthorizedAccessException)
                {
                    ErrorMessage = "Unauthorized Access";
                    return false;
                }
            }
        }

        /// <summary>
        /// Load all child directories as new nodes.
        /// </summary>
        public override void LoadChildren()
        {
            List<DirectoryInfo> directories = _directory.GetDirectories().ToList();
            List<FileInfo> files = _directory.GetFiles().ToList();

            // Add directories first
            foreach (DirectoryInfo d in directories)
            {
                DirectoryDataModel ddm = new DirectoryDataModel(d.FullName, _treeLevel + 1);
                ddm.Parent = this;
                Children.Add(ddm);
            }

            // Now add children
            foreach (FileInfo f in files)
            {
                DirectoryDataModel ddm = new DirectoryDataModel(f.FullName, _treeLevel + 1);
                ddm.Parent = this;
                Children.Add(ddm);
            }
        }
    }
}
