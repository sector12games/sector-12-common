﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DualProgressBarsTest.xaml.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Sector12CommonTest
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows;
    using Sector12Common.Controls.ControlData;

    /// <summary>
    /// Interaction logic for DualProgressBarsTest.xaml
    /// </summary>
    // ReSharper disable UnusedMember.Local
    public partial class DualProgressBarsTest : Window
    {
        private int _nextTaskCounter;
        private int _newTaskCounter;
        private int _dynamicTaskCounter;
        private bool _useOnTheFlySubtaskAssigment;

        /// <summary>
        /// Initializes a new instance of the <see cref="DualProgressBarsTest"/> class.
        /// </summary>
        public DualProgressBarsTest()
        {
            InitializeComponent();

            // There are three options for testing this class:
            // 1) Initialize everything (primary and subtasks) up front
            // 2) Initialize only primary tasks up front (not subtasks)
            // 3) Initialize everything in xaml
            // Comment both Initialize() lines below and uncomment the xaml code to 
            // test this class in xaml.
            InitializeOnlyPrimaryTasksUpFront();
            ////InitailizeEverythingUpFront();

            // We only need this if we're calling from a separate worker thread.
            ProgressBars.InitializeForUseInWorkerThread(Dispatcher);
        }

        /// <summary>
        /// Init everything up front.
        /// </summary>
        private void InitailizeEverythingUpFront()
        {
            ObservableCollection<ProgressBarSubtask> buildNpcsSubtasks = new ObservableCollection<ProgressBarSubtask>
            {
                new ProgressBarSubtask("Building NPCs Subtask 1", "Building NPCs Details 1"),
                new ProgressBarSubtask("Building NPCs Subtask 2", "Building NPCs Details 2"),
                new ProgressBarSubtask("Building NPCs Subtask 3", "Building NPCs Details 3")
            };

            ObservableCollection<ProgressBarSubtask> buildObjectsSubtasks = new ObservableCollection<ProgressBarSubtask>
            {
                new ProgressBarSubtask("Building Objects Subtask 1", "Building Objects Details 1"),
                new ProgressBarSubtask("Building Objects Subtask 2", "Building Objects Details 2"),
                new ProgressBarSubtask("Building Objects Subtask 3", "Building Objects Details 3")
            };

            ObservableCollection<ProgressBarTask> c = new ObservableCollection<ProgressBarTask>
            {
                new ProgressBarTask("Initialize Photoshop...", "Details", 0),
                new ProgressBarTask("Prepping Folders...", "Details", 0),
                new ProgressBarTask("Building NPCs...", "Details", buildNpcsSubtasks),
                new ProgressBarTask("Building Objects...", "Details", buildObjectsSubtasks)
            };
            ProgressBars.Tasks = c;
        }

        /// <summary>
        /// Init only primary tasks up front.
        /// </summary>
        private void InitializeOnlyPrimaryTasksUpFront()
        {
            ObservableCollection<ProgressBarTask> c = new ObservableCollection<ProgressBarTask>
            {
                new ProgressBarTask("Initialize Photoshop...", "Details", 0),
                new ProgressBarTask("Prepping Folders...", "Details", 0),
                new ProgressBarTask("Building NPCs...", "Details", 5),
                new ProgressBarTask("Building Objects...", "Details", 5)
            };
            ProgressBars.Tasks = c;

            _useOnTheFlySubtaskAssigment = true;
        }

        /// <summary>
        /// Move to the next task.
        /// </summary>
        private void NextTask()
        {
            _nextTaskCounter++;

            if (_useOnTheFlySubtaskAssigment)
            {
                ProgressBars.NextTask(
                    "Subtask on-the-fly " + _nextTaskCounter + "...",
                    "Details on-the-fly " + _nextTaskCounter);
            }
            else
            {
                ProgressBars.NextTask();
            }

            ProgressBars.PrintTotalTaskDebugInfo();
        }

        /// <summary>
        /// Event listener.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NextTask_Click(object sender, RoutedEventArgs e)
        {
            NextTask();

            ProgressBars.PrintTotalTaskDebugInfo();
        }

        /// <summary>
        /// Event listener.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FinalTask_Click(object sender, RoutedEventArgs e)
        {
            ProgressBars.FinalTask("Build Complete!");

            ProgressBars.PrintTotalTaskDebugInfo();
        }

        /// <summary>
        /// Event listener.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Reset_Click(object sender, RoutedEventArgs e)
        {
            _nextTaskCounter = 0;
            _newTaskCounter = 0;
            _dynamicTaskCounter = 0;
            ProgressBars.Reset(false);

            ProgressBars.PrintTotalTaskDebugInfo();
        }

        /// <summary>
        /// Event listener.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddNewTask_Click(object sender, RoutedEventArgs e)
        {
            _newTaskCounter++;
            ProgressBarTask task = new ProgressBarTask("New Task " + _newTaskCounter, "New Task Details " + _newTaskCounter, 5);
            ProgressBars.Tasks.Add(task);

            // Also move on to the next task
            NextTask();

            ProgressBars.PrintTotalTaskDebugInfo();
        }

        /// <summary>
        /// Event listener.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddDynamicTask_Click(object sender, RoutedEventArgs e)
        {
            _dynamicTaskCounter++;
            ProgressBarTask dt = new ProgressBarTask("Dynamic Task " + _dynamicTaskCounter, "Dynamic Task Details " + _dynamicTaskCounter, 5);
            ProgressBars.DynamicTasks.Add(dt);

            // Also move on to the next task
            NextTask();

            ProgressBars.PrintTotalTaskDebugInfo();
        }
    }

    // ReSharper restore UnusedMember.Local
}
