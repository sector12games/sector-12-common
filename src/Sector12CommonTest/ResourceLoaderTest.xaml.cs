﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ResourceLoaderTest.xaml.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Sector12CommonTest
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Xml.Linq;
    using Sector12Common.Helpers;
    using Sector12Common.ResourceLoaders;

    /// <summary>
    /// Interaction logic for ResourceLoaderTest.xaml
    /// </summary>
    // ReSharper disable UnusedMember.Local
    // ReSharper disable UnusedVariable
    public partial class ResourceLoaderTest : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceLoaderTest"/> class.
        /// </summary>
        public ResourceLoaderTest()
        {
            InitializeComponent();

            ////LoadString();
            ////LoadXDocument();
            ////LoadXamlControl();
            ////LoadImage();
            ////LoadImageControl();

            LoadMemoryStream();
            SaveMemoryStreamToTemporaryFile();
        }

        /// <summary>
        /// Loads a string resource.
        /// </summary>
        private void LoadString()
        {
            Uri uri = new Uri("pack://application:,,,/Content/ResourceLoaderTestFiles/simple_xml.xml");
            string s = WpfResourceLoader.LoadResource<string>(uri, ResourceLoaderContentType.Text);
        }

        /// <summary>
        /// Loads an xml document resource.
        /// </summary>
        private void LoadXDocument()
        {
            // Load an XDocument
            Uri uri = new Uri("pack://application:,,,/Content/ResourceLoaderTestFiles/simple_xml.xml");
            XDocument xdoc = WpfResourceLoader.LoadResource<XDocument>(uri, ResourceLoaderContentType.Xml);
        }

        /// <summary>
        /// Loads a xaml control resouce.
        /// </summary>
        private void LoadXamlControl()
        {
            // Load a Xaml control. If the file is a .xaml file, make sure to 
            // set it's build action to resource and custom tool to empty so
            // that it doesn't try to compile. Also, make sure the namespace
            // is defined somewhere in the xaml file, or things will blow up.
            Uri uri = new Uri("pack://application:,,,/Content/ResourceLoaderTestFiles/simple_xaml_control.xaml");
            TextBlock tb = WpfResourceLoader.LoadResource<TextBlock>(uri, ResourceLoaderContentType.Xaml);
        }

        /// <summary>
        /// Load an image resource (win forms).
        /// </summary>
        private void LoadImage()
        {
            // Load a System.Drawing.Image
            Uri uri = new Uri("pack://application:,,,/Content/ResourceLoaderTestFiles/collision.png");
            System.Drawing.Image image = WpfResourceLoader.LoadResource<System.Drawing.Image>(uri, ResourceLoaderContentType.Image);
        }

        /// <summary>
        /// Load an image control resource (wpf).
        /// </summary>
        private void LoadImageControl()
        {
            // Load a System.Windows.Controls.Image
            Uri uri = new Uri("pack://application:,,,/Content/ResourceLoaderTestFiles/collision.png");
            Image image = WpfResourceLoader.LoadResource<Image>(uri, ResourceLoaderContentType.ImageControl);
        }

        /// <summary>
        /// Load a file as a stream.
        /// </summary>
        private void LoadMemoryStream()
        {
            // Load any type of file as a stream
            Uri uri = new Uri("pack://application:,,,/Content/ResourceLoaderTestFiles/simple_xml.xml");
            MemoryStream stream = WpfResourceLoader.LoadResource<MemoryStream>(uri, ResourceLoaderContentType.MemoryStream);
        }

        /// <summary>
        /// Save a stream in memory to a tempoary file.
        /// </summary>
        private void SaveMemoryStreamToTemporaryFile()
        {
            // Load any type of file as a stream
            Uri uri = new Uri("pack://application:,,,/Content/ResourceLoaderTestFiles/simple_xml.xml");
            MemoryStream stream = WpfResourceLoader.LoadResource<MemoryStream>(uri, ResourceLoaderContentType.MemoryStream);

            // Create a new temporary file and save the memory stream to it.
            // We then have a useable System.IO.FileStream.
            // http://stackoverflow.com/questions/8624071/save-and-load-memorystream-to-from-a-file
            FileInfo file = FileSystemHelper.CreateTemporaryFileFromMemoryStream(stream, ".xml");
        }
    }
    // ReSharper restore UnusedVariable
    // ReSharper restore UnusedMember.Local
}
